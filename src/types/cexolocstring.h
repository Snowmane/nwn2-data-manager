#ifndef CEXOLOCSTRING_H
#define CEXOLOCSTRING_H

#include "types.h"
#include <string>
#include <map>

class CExoLocString
{
public:
    enum Gender{Male=0,Female=1};
    CExoLocString();
    virtual ~CExoLocString();

    bool loadFromData(const char* data);
    std::string getStringValue(DWORD languageID,Gender gender = CExoLocString::Male);

protected:
    DWORD _strRef;
    std::map<DWORD,std::string> _strings;
};

#endif // CEXOLOCSTRING_H
