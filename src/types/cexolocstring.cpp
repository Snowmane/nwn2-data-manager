#include "cexolocstring.h"

#include <sstream>

CExoLocString::CExoLocString()
{
}

CExoLocString::~CExoLocString()
{
}

bool CExoLocString::loadFromData(const char* data)
{
    DWORD totalSize = *((DWORD*)(data+0));
    _strRef = *((DWORD*)(data+4));
    DWORD strCount =  *((DWORD*)(data+8));
    const char* strpos = data+12;
    for(int strIndex = 0; strIndex < strCount; strIndex++)
    {
        DWORD stringId = (*((DWORD*)(strpos)));
        DWORD languageId = stringId / 2;
        DWORD stringGender = stringId & 0x00000001;
        strpos+=sizeof(DWORD);
        DWORD strSize = *((DWORD*)(strpos));
        strpos+=sizeof(DWORD);
        std::string strVal = std::string(strpos,strSize);
        _strings[stringId]=strVal;
        strpos+=strSize;
    }
    return true;
}

std::string CExoLocString::getStringValue(DWORD languageID,Gender gender)
{
    DWORD stringId = languageID*2+gender;
    if(_strings.find(stringId)!=_strings.end())
        return _strings[stringId];

    std::stringstream buff;
    buff << _strRef;

    return buff.str();
}
