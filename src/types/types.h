#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <string>

typedef uint8_t BYTE;
typedef char CHAR;
typedef uint16_t WORD;
typedef int16_t SHORT;
typedef uint32_t DWORD;
typedef int32_t INT;
typedef uint64_t DWORD64;
typedef int64_t INT64;
typedef float FLOAT;
typedef double DOUBLE;
typedef std::string CExoString;
typedef std::string CResRef;

#endif // TYPES_H
