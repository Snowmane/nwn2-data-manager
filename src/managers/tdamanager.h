#ifndef TDAMANAGER_H
#define TDAMANAGER_H

#include "../types/types.h"
#include "../data/file-formats/tdadata.h"
#include <string>
#include <vector>

class TDAManager
{
private:
    TDAManager();
    ~TDAManager(){}

public:
    static TDAManager& instance();

    void setNWN2Path(std::string path);

    bool get2DARowCount(std::string fileName,
                        DWORD &countOut);
    bool get2DAValue(std::string fileName,
                     std::string rowIndex,
                     std::string colName,
                     std::string& valueOut);
    bool get2DAValue(std::string fileName,
                     DWORD rowIndex,
                     std::string colName,
                     std::string& valueOut);
    bool get2DARow(std::string fileName,
                   DWORD rowIndex,
                   std::map<std::string,std::string>& row);
    bool get2DAColNames(std::string fileName,
                        std::vector<std::string>& colNames);
    bool has2DAColName(std::string fileName,std::string colName);

    void clearCache(){_2DAData.clear();}

protected:
    std::string _NWN2Path;
    std::map<std::string, TDAData> _2DAData;

    bool load2DAFile(std::string fileName);
};

#endif // TDAMANAGER_H
