#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <string>
#include <set>
#include <map>
#include "../types/types.h"

class ResourceManager
{
private:
    ResourceManager();
    ~ResourceManager(){}

public:
    static ResourceManager& instance();

    bool setInstallDirPath(std::string path){_installDirPath=path; return true;}
    std::string installDirPath() const {return _installDirPath;}
    void reset();

    enum ResourceType
    {
        RT_INVALID = 0xffff,
        RT_BMP = 1,
        RT_TGA = 3,
        RT_WAV = 4,
        RT_PLT = 6,
        RT_INI = 7,
        RT_TXT = 10,
        RT_MDL = 2002,
        RT_NSS = 2009,
        RT_NCS = 2010,
        RT_ARE = 2012,
        RT_SET = 2013,
        RT_IFO = 2014,
        RT_BIC = 2015,
        RT_WOK = 2016,
        RT_2DA = 2017,
        RT_TXI = 2022,
        RT_GIT = 2023,
        RT_UTI = 2025,
        RT_UTC = 2027,
        RT_DLG = 2029,
        RT_ITP = 2030,
        RT_UTT = 2032,
        RT_DDS = 2033,
        RT_UTS = 2035,
        RT_LTR = 2036,
        RT_GFF = 2037,
        RT_FAC = 2038,
        RT_UTE = 2040,
        RT_UTD = 2042,
        RT_UTP = 2044,
        RT_DFT = 2045,
        RT_GIC = 2046,
        RT_GUI = 2047,
        RT_UTM = 2051,
        RT_DWK = 2052,
        RT_PWK = 2053,
        RT_JRL = 2056,
        RT_UTW = 2058,
        RT_SSF = 2060,
        RT_NDB = 2064,
        RT_PTM = 2065,
        RT_PTT = 2066
    };

    std::set<std::string> getResourceList(ResourceType resourceType);
    bool loadResourceFile(std::string fileName, char*& fileData, size_t &fileLength);
    ResourceType resourceTypeFromExtension(std::string extension);
    std::string extensionFromResourceType(WORD type);
    bool isTemplateType(ResourceType resourceType);

protected:
    std::string _installDirPath;
    std::map<std::string, std::set<std::string> > _hakEntries;

    void getResourceListFromHaks(ResourceType resourceType, std::set<std::string>& resourceList);
    void getResourceListFromOverrides(ResourceType resourceType, std::set<std::string>& resourceList);
    void getResourceListFromCampaign(ResourceType resourceType, std::set<std::string>& resourceList);
    void getResourceListFromModule(ResourceType resourceType, std::set<std::string>& resourceList);
    void getResourceListFromInstall(ResourceType resourceType, std::set<std::string>& resourceList);

    enum LoadResult{LOADED,NOT_FOUND,ERROR};
    LoadResult loadResourceFileFromHaks(std::string fileName, char *&fileData, size_t& fileLength);
    LoadResult loadResourceFileFromOverrides(std::string fileName, char *&fileData, size_t& fileLength);
    LoadResult loadResourceFileFromCampaign(std::string fileName, char *&fileData, size_t& fileLength);
    LoadResult loadResourceFileFromModule(std::string fileName, char *&fileData, size_t& fileLength);
    LoadResult loadResourceFileFromInstall(std::string fileName, char *&fileData, size_t& fileLength);
    std::set<std::string> getEntriesForHak(std::string hakName);

};

#endif // RESOURCEMANAGER_H
