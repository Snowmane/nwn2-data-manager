#ifndef BLUEPRINTMANAGER_H
#define BLUEPRINTMANAGER_H

#include <string>
#include <map>

// forward decs
class Creature;
class Door;
class Encounter;
class Item;
class Placeable;
class Sound;
class Store;
class Trigger;
class Waypoint;

class BlueprintManager
{
private:
    BlueprintManager();
    ~BlueprintManager(){}

public:
    static BlueprintManager& instance();
    bool loadBlueprints();
    void clearBlueprints();

    bool getItemByResRef(std::string resRef, Item& itemOut) const;
    bool getItemByResourceName(std::string resourceName, Item& itemOut) const;



protected:
    bool _isLoaded;
    template<class T> bool loadTypedBlueprints(std::map<std::string,T*>& resRefMap,std::map<std::string,T*>& resourceMap, std::string extension);
    template<class T> void clearTypedBlueprints(std::map<std::string,T*>& resRefMap,std::map<std::string,T*>& resourceMap);
    bool loadCreatureBlueprints();
    bool loadDoorBlueprints();
    bool loadEncounterBlueprints();
    bool loadItemBlueprints();
    bool loadPlaceableBlueprints();
    bool loadSoundBlueprints();
    bool loadStoreBlueprints();
    bool loadTriggerBlueprints();
    bool loadWaypointBlueprints();
    std::map<std::string,Creature*>  _creaturessByResRef;
    std::map<std::string,Creature*>  _creaturesByResourceName;
    std::map<std::string,Door*>      _doorsByResRef;
    std::map<std::string,Door*>      _doorsByResourceName;
    std::map<std::string,Encounter*> _encountersByResRef;
    std::map<std::string,Encounter*> _encountersByResourceName;
    std::map<std::string,Item*>      _itemsByResRef;
    std::map<std::string,Item*>      _itemsByResourceName;
    std::map<std::string,Placeable*> _placeablesByResRef;
    std::map<std::string,Placeable*> _placeablesByResourceName;
    std::map<std::string,Sound*>     _soundsByResRef;
    std::map<std::string,Sound*>     _soundsByResourceName;
    std::map<std::string,Store*>     _storesByResRef;
    std::map<std::string,Store*>     _storesByResourceName;
    std::map<std::string,Trigger*>   _triggersByResRef;
    std::map<std::string,Trigger*>   _triggersByResourceName;
    std::map<std::string,Waypoint*>  _waypointsByResRef;
    std::map<std::string,Waypoint*>  _waypointsByResourceName;
};

#endif // BLUEPRINTMANAGER_H
