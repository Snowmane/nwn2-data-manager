#include "resourcemanager.h"
#include "modulemanager.h"
#include "../structs/erfheader.h"
#include "../structs/erfkey.h"
#include "../structs/erfresource.h"
#include "../utils/fileutils.h"
#include "../utils/stringutils.h"
#include "../utils/errorutils.h"
#include "../utils/erfutils.h"
#include "../data/file-formats/ifodata.h"


ResourceManager::ResourceManager()
{
}

ResourceManager& ResourceManager::instance()
{
    static ResourceManager inst;
    //TODO: We should not init/set _installDirPath here!!!
    inst.setInstallDirPath("C:/Program Files (x86)/Atari/Neverwinter Nights 2");
    return inst;
}

std::set<std::string> ResourceManager::getEntriesForHak(std::string hakName)
{
    // look for the entries in our cache. If it is not found,
    // then load it from the disk;
    if(_hakEntries.find(hakName)==_hakEntries.end())
    {
        std::string hakPath=ModuleManager::instance().getNWN2Datapath()+"hak/"+hakName;
        std::set<std::string> entryList;
        ErfUtils::getFileListFromERF(hakPath,entryList);
        _hakEntries[hakName] = entryList;
    }
    return _hakEntries[hakName];
}

void ResourceManager::reset()
{
    _installDirPath="";
    _hakEntries.clear();
}

std::set<std::string> ResourceManager::getResourceList(ResourceType resourceType)
{
    std::set<std::string> resourceList;

    if(!isTemplateType(resourceType))
        getResourceListFromHaks(resourceType, resourceList);

    getResourceListFromOverrides(resourceType, resourceList);
    getResourceListFromCampaign(resourceType, resourceList);
    getResourceListFromModule(resourceType, resourceList);
    getResourceListFromInstall(resourceType, resourceList);

    return resourceList;
}

void ResourceManager::getResourceListFromHaks(ResourceType resourceType, std::set<std::string>& resourceList)
{
    std::string nwn2DataPath = ModuleManager::instance().getNWN2Datapath();
    std::set<std::string> hakEntries;

    for(unsigned int index=0; index < ModuleManager::instance().ifoData()->modHakList().size(); index++)
    {
        CExoString hakName = ModuleManager::instance().ifoData()->modHakList().at(index) + ".hak";
        hakEntries = ResourceManager::instance().getEntriesForHak(hakName);
        std::set<std::string>::iterator iter;
        for(iter = hakEntries.begin(); iter != hakEntries.end(); ++iter)
        {
            if(resourceType== resourceTypeFromExtension((FileUtils::getFileExtension(*iter))))
                resourceList.insert(*iter);
        }
    }
}

void ResourceManager::getResourceListFromOverrides(ResourceType resourceType, std::set<std::string>& resourceList)
{
    std::vector<std::string> overridePaths;
    overridePaths.push_back(ModuleManager::instance().getNWN2Datapath()+"/override");
    overridePaths.push_back(_installDirPath+"/override");

    for(size_t i = 0; i < overridePaths.size(); i++)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries(overridePaths[i],"2da");
        for(size_t i = 0; i < entries.size(); i++)
        {
            if(resourceType== resourceTypeFromExtension((FileUtils::getFileExtension(entries[i]))))
                resourceList.insert(entries[i]);
        }
    }
}

void ResourceManager::getResourceListFromCampaign(ResourceType resourceType, std::set<std::string>& resourceList)
{
    //TODO:
}

void ResourceManager::getResourceListFromModule(ResourceType resourceType, std::set<std::string>& resourceList)
{
    if(ModuleManager::instance().modFile()=="")
    {
        // it's a directory
        std::vector<std::string> entries = FileUtils::getDirEntries(ModuleManager::instance().modPath(),extensionFromResourceType(resourceType));
        for(size_t i = 0; i < entries.size(); i++)
            resourceList.insert(entries[i]);
    }
    else
    {
        // it's a .mod(ERF) file
        std::set<std::string> fileList;
        if(ErfUtils::getFileListFromERF(ModuleManager::instance().modPath()+ModuleManager::instance().modFile(),fileList))
        {
            std::set<std::string>::iterator iter;
            for(iter = fileList.begin(); iter != fileList.end(); ++iter)
            {
                if(resourceType== resourceTypeFromExtension((FileUtils::getFileExtension(*iter))))
                    resourceList.insert(*iter);
            }
        }
        else
        {
            //TODO: error
            bool crap = true;
        }
    }
}

void ResourceManager::getResourceListFromInstall(ResourceType resourceType, std::set<std::string>& resourceList)
{
    //TODO: FOR NOW, CHEAT ON PATHS

    // LOOK IN A FOLDER WHERE THE BASE 2DA'S HAVE BEEN EXTRACTED
    if(resourceType == RT_2DA)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries("C:/Users/James/Desktop/2DA","2da");
        for(size_t i = 0; i < entries.size(); i++)
            resourceList.insert(entries[i]);
    }
    // LOOK IN A FOLDER WHERE THE BASE TEMPLATES HAVE BEEN EXTRACTED
    else if(isTemplateType(resourceType))
    {
        std::vector<std::string> entries = FileUtils::getDirEntries("C:/NWN2GlobalTemplates",extensionFromResourceType(resourceType));
        for(size_t i = 0; i < entries.size(); i++)
            resourceList.insert(entries[i]);
    }
}


bool ResourceManager::loadResourceFile(std::string fileName, char *&fileData, size_t& fileLength)
{
    // hak
    // override (edits in toolset are saved here)
    // base zip files

    // If we haven't already, then look in the mod
    // and get the list of haks and the campaign.
    if(ModuleManager::instance().ifoData() == NULL)
    {
        ErrorUtils::logErrorToConsole("TDAManager::load2DAFile : module not loaded");
        return false;
    }

    // loop over the haks (unless this is a blueprint)
    if(!isTemplateType(resourceTypeFromExtension(FileUtils::getFileExtension(fileName))))
    {
        if(loadResourceFileFromHaks(fileName,fileData,fileLength) == LOADED)
            return true;
    }

    // look in the overrides (My docs first, then the install)
    if(loadResourceFileFromOverrides(fileName,fileData,fileLength) == LOADED)
        return true;
/*
    // look though the base game zip files (the install)
    if(loadResourceFileFromCampaign(fileName,fileData,fileLength) == LOADED)
        return true;
*/
    // look though the base game zip files (the install)
    if(loadResourceFileFromModule(fileName,fileData,fileLength) == LOADED)
        return true;

    // look though the base game zip files (the install)
    if(loadResourceFileFromInstall(fileName,fileData,fileLength) == LOADED)
        return true;

    return false;
}

ResourceManager::LoadResult ResourceManager::loadResourceFileFromHaks(std::string fileName, char *&fileData, size_t& fileLength)
{
    std::string nwn2DataPath = ModuleManager::instance().getNWN2Datapath();
    std::set<std::string> hakEntries;

    for(unsigned int index=0; index < ModuleManager::instance().ifoData()->modHakList().size(); index++)
    {
        CExoString hakName = ModuleManager::instance().ifoData()->modHakList().at(index) + ".hak";
        hakEntries = ResourceManager::instance().getEntriesForHak(hakName);

        if(hakEntries.find(fileName) == hakEntries.end())
            continue;

        std::string erfPath=nwn2DataPath+"hak/"+hakName;
        if(ErfUtils::getResourceFileFromERF(erfPath, fileName, fileData, fileLength))
            return LOADED;
        else
            return ERROR;
    }
    return NOT_FOUND;
}

ResourceManager::LoadResult ResourceManager::loadResourceFileFromOverrides(std::string fileName, char *&fileData, size_t& fileLength)
{
    std::vector<std::string> overridePaths;
    overridePaths.push_back(ModuleManager::instance().getNWN2Datapath()+"/override");
    overridePaths.push_back(_installDirPath+"/override");

    for(size_t i = 0; i < overridePaths.size(); i++)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries(overridePaths[i],"2da");
        for(size_t i = 0; i < entries.size(); i++)
        {
            if(entries[i] == fileName)
            {
                fileData = FileUtils::getFileData(overridePaths[i]+"/"+fileName,&fileLength);
                if(fileData == NULL)
                    return ERROR;
                else
                    return LOADED;
            }
        }
    }
    return NOT_FOUND;
}

ResourceManager::LoadResult ResourceManager::loadResourceFileFromCampaign(std::string fileName, char *&fileData, size_t& fileLength)
{
    return NOT_FOUND;
}

ResourceManager::LoadResult ResourceManager::loadResourceFileFromModule(std::string fileName, char *&fileData, size_t& fileLength)
{
    if(ModuleManager::instance().modFile()=="")
    {
        // it's a directory
        std::vector<std::string> entries = FileUtils::getDirEntries(ModuleManager::instance().modPath(), FileUtils::getFileExtension(fileName));
        for(size_t i = 0; i < entries.size(); i++)
        {
            if(entries[i] == fileName)
            {
                fileData = FileUtils::getFileData(ModuleManager::instance().modPath()+fileName,&fileLength);
                if(fileData == NULL)
                    return ERROR;
                else
                    return LOADED;
            }
        }
    }
    else
    {
        // it's a .mod(ERF) file
        std::set<std::string> fileList;
        std::string pathToErf = ModuleManager::instance().modPath()+ModuleManager::instance().modFile();
        if(ErfUtils::getFileListFromERF(pathToErf,fileList))
        {
            if(fileList.find(fileName)!=fileList.end())
            {
                if(!ErfUtils::getResourceFileFromERF(pathToErf,fileName,fileData,fileLength))
                    return ERROR;
                else
                    return LOADED;
            }
        }
        else
        {
            //TODO: error
            bool crap = true;
        }
    }
    return NOT_FOUND;
}


ResourceManager::LoadResult ResourceManager::loadResourceFileFromInstall(std::string fileName, char *&fileData, size_t& fileLength)
{
    //TODO: FOR NOW, CHEAT ON PATHS

    ResourceType resourceType = resourceTypeFromExtension(FileUtils::getFileExtension(fileName));

    // LOOK IN A FOLDER WHERE THE BASE 2DA'S HAVE BEEN EXTRACTED
    if(resourceType == RT_2DA)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries("C:/Users/James/Desktop/2DA","2da");
        for(size_t i = 0; i < entries.size(); i++)
        {
            if(entries[i] == fileName)
            {
                fileData = FileUtils::getFileData("C:/Users/James/Desktop/2DA/"+fileName,&fileLength);
                if(fileData == NULL)
                    return ERROR;
                else
                    return LOADED;
            }
        }
    }
    // LOOK IN A FOLDER WHERE THE BASE TEMPLATES HAVE BEEN EXTRACTED
    else if(resourceType == RT_UTI ||
            resourceType == RT_UTP)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries("C:/NWN2GlobalTemplates",FileUtils::getFileExtension(fileName));
        for(size_t i = 0; i < entries.size(); i++)
        {
            if(entries[i] == fileName)
            {
                fileData = FileUtils::getFileData("C:/NWN2GlobalTemplates/"+fileName,&fileLength);
                if(fileData == NULL)
                    return ERROR;
                else
                    return LOADED;
            }
        }
    }

    return NOT_FOUND;
}

ResourceManager::ResourceType ResourceManager::resourceTypeFromExtension(std::string extension)
{
    extension = StringUtils::toLower(extension);
    if(extension=="bmp"){return RT_BMP;}
    else if(extension=="tga"){return RT_TGA;}
    else if(extension=="wav"){return RT_WAV;}
    else if(extension=="plt"){return RT_PLT;}
    else if(extension=="ini"){return RT_INI;}
    else if(extension=="txt"){return RT_TXT;}
    else if(extension=="mdl"){return RT_MDL;}
    else if(extension=="nss"){return RT_NSS;}
    else if(extension=="ncs"){return RT_NCS;}
    else if(extension=="are"){return RT_ARE;}
    else if(extension=="set"){return RT_SET;}
    else if(extension=="ifo"){return RT_IFO;}
    else if(extension=="bic"){return RT_BIC;}
    else if(extension=="wok"){return RT_WOK;}
    else if(extension=="2da"){return RT_2DA;}
    else if(extension=="txi"){return RT_TXI;}
    else if(extension=="git"){return RT_GIT;}
    else if(extension=="uti"){return RT_UTI;}
    else if(extension=="utc"){return RT_UTC;}
    else if(extension=="dlg"){return RT_DLG;}
    else if(extension=="itp"){return RT_ITP;}
    else if(extension=="uti"){return RT_UTT;}
    else if(extension=="dds"){return RT_DDS;}
    else if(extension=="uts"){return RT_UTS;}
    else if(extension=="ltr"){return RT_LTR;}
    else if(extension=="gff"){return RT_GFF;}
    else if(extension=="fac"){return RT_FAC;}
    else if(extension=="ute"){return RT_UTE;}
    else if(extension=="utd"){return RT_UTD;}
    else if(extension=="utp"){return RT_UTP;}
    else if(extension=="dft"){return RT_DFT;}
    else if(extension=="gic"){return RT_GIC;}
    else if(extension=="gui"){return RT_GUI;}
    else if(extension=="utm"){return RT_UTM;}
    else if(extension=="dwk"){return RT_DWK;}
    else if(extension=="pwk"){return RT_PWK;}
    else if(extension=="jrl"){return RT_JRL;}
    else if(extension=="utw"){return RT_UTW;}
    else if(extension=="ssf"){return RT_SSF;}
    else if(extension=="ndb"){return RT_NDB;}
    else if(extension=="ptm"){return RT_PTM;}
    else if(extension=="ptt"){return RT_PTT;}

    return RT_INVALID;
}

std::string ResourceManager::extensionFromResourceType(WORD type)
{
    switch(type)
    {
    case RT_BMP:{return "bmp";}
    case RT_TGA:{return "tga";}
    case RT_WAV:{return "wav";}
    case RT_PLT:{return "plt";}
    case RT_INI:{return "ini";}
    case RT_TXT:{return "txt";}
    case RT_MDL:{return "mdl";}
    case RT_NSS:{return "nss";}
    case RT_NCS:{return "ncs";}
    case RT_ARE:{return "are";}
    case RT_SET:{return "set";}
    case RT_IFO:{return "ifo";}
    case RT_BIC:{return "bic";}
    case RT_WOK:{return "wok";}
    case RT_2DA:{return "2da";}
    case RT_TXI:{return "txi";}
    case RT_GIT:{return "git";}
    case RT_UTI:{return "uti";}
    case RT_UTC:{return "utc";}
    case RT_DLG:{return "dlg";}
    case RT_ITP:{return "itp";}
    case RT_UTT:{return "uti";}
    case RT_DDS:{return "dds";}
    case RT_UTS:{return "uts";}
    case RT_LTR:{return "ltr";}
    case RT_GFF:{return "gff";}
    case RT_FAC:{return "fac";}
    case RT_UTE:{return "ute";}
    case RT_UTD:{return "utd";}
    case RT_UTP:{return "utp";}
    case RT_DFT:{return "dft";}
    case RT_GIC:{return "gic";}
    case RT_GUI:{return "gui";}
    case RT_UTM:{return "utm";}
    case RT_DWK:{return "dwk";}
    case RT_PWK:{return "pwk";}
    case RT_JRL:{return "jrl";}
    case RT_UTW:{return "utw";}
    case RT_SSF:{return "ssf";}
    case RT_NDB:{return "ndb";}
    case RT_PTM:{return "ptm";}
    case RT_PTT:{return "ptt";}
    }
    return "";
}

bool ResourceManager::isTemplateType(ResourceType resourceType)
{
    return (resourceType == RT_UTC ||
            resourceType == RT_UTD ||
            resourceType == RT_UTE ||
            resourceType == RT_UTI ||
            resourceType == RT_UTM ||
            resourceType == RT_UTP ||
            resourceType == RT_UTS ||
            resourceType == RT_UTT ||
            resourceType == RT_UTW);
}
