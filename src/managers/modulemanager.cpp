#include "modulemanager.h"

#include "../data/file-formats/ifodata.h"
#include "../data/file-formats/gffdata.h"
#include "../utils/errorutils.h"
#include "../utils/fileutils.h"
#include "../utils/stringutils.h"
#include "tdamanager.h"
#include "resourcemanager.h"

ModuleManager::ModuleManager():
    _ifoData(NULL)
{
}

ModuleManager& ModuleManager::instance()
{
    static ModuleManager inst;
    return inst;
}

void ModuleManager::resetMembers()
{
    if(_ifoData != NULL)
    {
        delete _ifoData;
        _ifoData = NULL;
    }
    ResourceManager::instance().reset();
}

bool ModuleManager::setModPath(std::string path)
{
    //clear cache data
    resetMembers();

    // prepare the path & file information
    FileUtils::EntryType type = FileUtils::entryType(path);
    if(type == FileUtils::ET_DIR)
    {
        char lastChar = StringUtils::lastChar(path);
        if( lastChar != '/' && lastChar != '\\')
            path += "/";

        _modPath = path;
        _modFile = "";
    }
    else if(type == FileUtils::ET_FILE)
    {
        // extract dir path
        _modPath = FileUtils::getPathToFile(path);

        // extract the file name
        _modFile = path.substr(_modPath.size()-1);
    }
    TDAManager::instance().clearCache();

    // load the IFO
    GFFData gffIFO;
    if(_modFile != "")
    {
        // get the IFO data from the mod ERF
        std::string modFileName = _modPath+_modFile;

        // load the ifo data
        char* ifoData = NULL;
        size_t ifoDataLength = 0;
/*
        if(!IEncapsulatedResourceFile::getFileFromERF(modFileName,"MODULE.IFO", ifoData,ifoDataLength))
        {
            // error
            ErrorUtils::logErrorToConsole("ModuleManager::setModPath : unable to extract MODULE.IFO from module");
            return false;
        }
        if(!gffIFO.loadFromData(ifoData,ifoDataLength,modFileName))
        {
            //TODO: error
            return false;
        }
*/
    }
    else
    {
        if(_modPath == "")
        {
            // error
            ErrorUtils::logErrorToConsole("ModuleManager::setModPath : _modPath is empty");
            return false;
        }

        // load the IFO file data from the directory structure
        std::string fullPath = _modPath+"MODULE.IFO";
        if(!gffIFO.loadFromFile(fullPath))
        {
            //TODO: error
            return false;
        }
    }

    // load the mod IFO
    _ifoData = new IFOData();
    if(!_ifoData->loadFromGFF(gffIFO))
    {
        // TODO: error
        ErrorUtils::logErrorToConsole("ModuleManager::setModPath : failed to parse MODULE.IFO");
        delete _ifoData;
        _ifoData = NULL;
        return false;
    }

    return true;
}

std::string ModuleManager::getNWN2Datapath() const
{
    if(_modFile=="")
        return _modPath+"../../";

    return _modPath+"../";
}


