#include "tlkmanager.h"

#include <stdlib.h>

TLKManager& TLKManager::instance()
{
    static TLKManager inst;
    return inst;
}

bool TLKManager::loadDialogTlk(std::string filePath)
{
    bool tlkLoaded =_dialogTlk.loadFileData(filePath);
    if(tlkLoaded)
    {
        if(filePath.find_last_of(".")==std::string::npos)
            _dialogTlkF.loadFileData(filePath+"f");
        else
            _dialogTlkF.loadFileData(filePath.substr(0,filePath.find_last_of("."))+"f"+
                                     filePath.substr(filePath.find_last_of("."),filePath.length()-filePath.find_last_of(".")));
    }
    return  tlkLoaded;
}

bool TLKManager::loadCustomTlk(std::string filePath)
{
    return _customTlk.loadFileData(filePath);
}

bool TLKManager::getTlkEntry(std::string strRef, TLKDataElement& out)
{
    return getTlkEntry(atol(strRef.c_str()), out);
}

bool TLKManager::getTlkEntry(std::string strRef, Gender gender, TLKDataElement& out)
{
    return getTlkEntry(atol(strRef.c_str()), gender, out);
}


bool TLKManager::getTlkEntry(DWORD strRef, TLKDataElement& out)
{
    return getTlkEntry(strRef, MALE, out);
}

bool TLKManager::getTlkEntry(unsigned int strRef, Gender gender, TLKDataElement& out)
{
    if(strRef <= 0x01FFFFFF)
    {
        if(strRef >= 0x01000000)
        {
            unsigned int altStrRef = strRef & 0x00FFFFFF;
            // try to retrieve from the alternate TLK
            if(gender == MALE)
            {
                if(_customTlk.getTLKDataElement(altStrRef, out))
                    return true;
            }
            else
            {
                if(_customTlkF.getTLKDataElement(altStrRef, out))
                    return true;
            }
            bool crap = true;
        }

        // if the alternate lookup failed or this is standard,
        // try to retrieve from the standard tlk
        if(gender == MALE)
        {
            if(_dialogTlk.getTLKDataElement(strRef & 0x00FFFFFF, out))
                return true;
        }
        else
        {
            if(_dialogTlkF.getTLKDataElement(strRef & 0x00FFFFFF, out))
                return true;
        }
    }

    // if we don;t find an entry, then set an empty string and return false;
    out.stringValue="";
    return false;
}

void TLKManager::reset()
{
    _dialogTlk.reset();
    _dialogTlkF.reset();
    _customTlk.reset();
    _customTlkF.reset();
}
