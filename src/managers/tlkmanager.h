#ifndef TLKMANAGER_H
#define TLKMANAGER_H

#include "../data/file-formats/tlkdata.h"
#include "../types/types.h"
#include <string>

class TLKManager
{
private:
    TLKManager(){}
    ~TLKManager(){}
public:
    enum Gender{MALE, FEMALE};
    static TLKManager& instance();
    bool loadDialogTlk(std::string filePath);
    bool loadCustomTlk(std::string filePath);
    bool getTlkEntry(std::string strRef, TLKDataElement& out);
    bool getTlkEntry(std::string strRef, Gender gender, TLKDataElement& out);
    bool getTlkEntry(DWORD strRef, TLKDataElement& out);
    bool getTlkEntry(DWORD strRef, Gender gender, TLKDataElement& out);

    void reset();

protected:
    TLKData _dialogTlk;
    TLKData _dialogTlkF;
    TLKData _customTlk;
    TLKData _customTlkF;
};

#endif // TLKMANAGER_H
