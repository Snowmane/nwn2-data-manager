#include "blueprintmanager.h"

#include "modulemanager.h"
#include "resourcemanager.h"
#include "../data/games-objects/item.h"
#include "../utils/fileutils.h"
#include "../utils/errorutils.h"

BlueprintManager::BlueprintManager():
    _isLoaded(false)
{
}

BlueprintManager& BlueprintManager::instance()
{
    static BlueprintManager inst;
    return inst;
}

bool BlueprintManager::getItemByResRef(std::string resRef, Item &itemOut) const
{
    if(_itemsByResRef.find(resRef) != _itemsByResRef.end())
    {
        itemOut = *(_itemsByResRef.at(resRef));
        return true;
    }
    return false;
}
bool BlueprintManager::getItemByResourceName(std::string resourceName, Item &itemOut) const
{
    if(_itemsByResourceName.find(resourceName) != _itemsByResourceName.end())
    {
        itemOut = *(_itemsByResourceName.at(resourceName));
        return true;
    }
    return false;
}

void BlueprintManager::clearBlueprints()
{
  /*clearTypedBlueprints<Creature>(_creaturessByResRef.clear(),
                                   _creaturesByResourceName.clear();
    clearTypedBlueprints<Door>(_doorsByResRef.clear(),
                               _doorsByResourceName.clear();
    clearTypedBlueprints<Encounter>(_encountersByResRef.clear(),
                                    _encountersByResourceName.clear();*/
    clearTypedBlueprints<Item>(_itemsByResRef,
                               _itemsByResourceName);
  /*clearTypedBlueprints<Placeable>(_placeablesByResRef.clear(),
                                    _placeablesByResourceName.clear();
    clearTypedBlueprints<Sound>(_soundsByResRef.clear(),
                                _soundsByResourceName.clear();
    clearTypedBlueprints<Store>(_storesByResRef.clear(),
                                _storesByResourceName.clear();
    clearTypedBlueprints<Trigger>(_triggersByResRef.clear(),
                                  _triggersByResourceName.clear();
    clearTypedBlueprints<Waypoint>(_waypointsByResRef.clear(),
                                   _waypointsByResourceName.clear();*/
}

template<class T> void BlueprintManager::clearTypedBlueprints(std::map<std::string,T*>& resRefMap,
                                                              std::map<std::string,T*>& resourceMap)
{
    // the resource maps will always have a reference to all of
    // the loaded blueprints, so delete them from there
    typename std::map<std::string,T*>::iterator iter;
    for(iter = resourceMap.begin(); iter != resourceMap.end(); ++iter)
        delete iter->second;

    resRefMap.clear();
    resourceMap.clear();
}

//TODO: make this threaded? but that would add a dependency to boost, which is probably ok.
bool BlueprintManager::loadBlueprints()
{
    if(!ModuleManager::instance().isLoaded())
    {
        ErrorUtils::logErrorToConsole(" BlueprintManager::loadBlueprints - module not loaded");
        return false;
    }
    if(ResourceManager::instance().installDirPath() == "")
    {
        ErrorUtils::logErrorToConsole(" BlueprintManager::loadBlueprints - installDirPath not set");
        return false;
    }

    //if(!loadCreatureBlueprints()){clearBlueprints();return false;}
    //if(!loadDoorBlueprints()){clearBlueprints();return false;}
    //if(!loadEncounterBlueprints()){clearBlueprints();return false;}
    if(!loadTypedBlueprints<Item>(_itemsByResRef, _itemsByResourceName, "uti"))
    {
        clearBlueprints();
        return false;
    }
  /*if(!loadTypedBlueprints<Placeable>(_placeablesByResRef, _placeablesByResourceName, "utp"))
    {
        clearBlueprints();
        return false;
    }*/
    //if(!loadSoundBlueprints()){clearBlueprints();return false;}
    //if(!loadStoreBlueprints()){clearBlueprints();return false;}
    //if(!loadTriggerBlueprints()){clearBlueprints();return false;}
    //if(!loadWaypointBlueprints()){clearBlueprints();return false;}

    return true;
}

template<class T>
bool BlueprintManager::loadTypedBlueprints(std::map<std::string,T*>& resRefMap,
                                           std::map<std::string,T*>& resourceMap,
                                           std::string extension)
{
    char* fileData;
    size_t fileLength;
    std::set<std::string>::iterator bpIter;
    std::set<std::string> bpList = ResourceManager::instance().getResourceList(ResourceManager::instance().resourceTypeFromExtension(extension));

    for(bpIter = bpList.begin(); bpIter != bpList.end(); ++bpIter)
    {
        if(ResourceManager::instance().loadResourceFile(*bpIter, fileData, fileLength))
        {
            GFFData gffData;
            if(!gffData.loadFromData(fileData, fileLength))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - failed to load " + *bpIter + " from the Resource Manager");
                delete[] fileData;
                continue;
            }
            T* bpObj = new T();
            if(!bpObj->loadFromGFFData(gffData))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - failed to create object for " + *bpIter);
                delete bpObj;
                delete[] fileData;
                continue;
            }
            if(bpObj->templateResRef()!="" &&
               resRefMap.find(bpObj->templateResRef()) != resRefMap.end())
            {
                // an item with this resref but a different resourcename has already been found!
                // log error and do not add this item.
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - The ResRef " + bpObj->templateResRef() + " is already used by another " + extension + " resource!");
                delete bpObj;
                delete[] fileData;
                continue;
            }
            // add it to the maps
            resourceMap[*bpIter]=bpObj;
            resRefMap[bpObj->templateResRef()]=bpObj;
            delete[] fileData;
        }
        else
        {
            // TODO: error - but this should never happen
            bool crap = true;
        }
    }

    // blueprints don't work in hak's - so don't bother looking

    // Try to load any blueprints from the override folder(My Docs then install)
    std::vector<std::string> overridePaths;
    overridePaths.push_back(ModuleManager::instance().getNWN2Datapath()+"/override");
    overridePaths.push_back(ResourceManager::instance().installDirPath()+"/override");

    for(size_t i = 0; i < overridePaths.size(); i++)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries(overridePaths[i],extension);
        for(size_t i = 0; i < entries.size(); i++)
        {
            // continue if we have already loaded this resource
            if(resourceMap.find(entries[i]) != resourceMap.end())
                continue;

            GFFData gffData;
            if(!gffData.loadFromFile(overridePaths[i]+"/"+entries[i]))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - failed to load " + entries[i] + " from " + overridePaths[i]);
                continue;
            }
            T* bpObj = new T();
            if(!bpObj->loadFromGFFData(gffData))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - failed to create object for " + entries[i]);
                delete bpObj;
                continue;
            }
            if(bpObj->templateResRef()!="" &&
               resRefMap.find(bpObj->templateResRef()) != resRefMap.end())
            {
                // an item with this resref but a different resourcename has already been found!
                // log error and do not add this item.
                ErrorUtils::logErrorToConsole("BlueprintManager::loadBlueprints() - The ResRef " + bpObj->templateResRef() + " is already used by another " + extension + " resource!");
                delete bpObj;
                continue;
            }
            // add it to the maps
            resourceMap[entries[i]]=bpObj;
            resRefMap[bpObj->templateResRef()]=bpObj;
        }
    }

    //TODO: Try to load the blueprint from the campaign referenced by the module;
    //_ifoData->capaignid
    //std::string campaignPath=getNWN2Datapath()+"hak/"+hakName;

    // Try to load the blueprint from the module
    if(ModuleManager::instance().modFile() == "")
    {
        // module dir
    }
    else
    {
        // module file
        std::string resourceList;
        //if(!IEncapsulatedResourceFile::getFileListForResourceTypeFromERF())
        //    bool crap = true;
    }

    // Try to load the blueprint from the templates


    return true;
}

bool  BlueprintManager::loadCreatureBlueprints()
{
    return true;
}

bool  BlueprintManager::loadDoorBlueprints()
{
    return true;
}

bool  BlueprintManager::loadEncounterBlueprints()
{
    return true;
}

//TODO: create template function to perform these tasks
bool BlueprintManager::loadItemBlueprints()
{
    // blueprints don't work in hak's - so don't bother looking

    // Try to load the blueprint from the override folder(My Docs then install)
    std::vector<std::string> overridePaths;
    overridePaths.push_back(ModuleManager::instance().getNWN2Datapath()+"/override");
    //TODO: overridePaths.push_back(nwn2InstallPath+"/override");

    for(size_t i = 0; i < overridePaths.size(); i++)
    {
        std::vector<std::string> entries = FileUtils::getDirEntries(overridePaths[i],"uti");
        for(size_t i = 0; i < entries.size(); i++)
        {
            // continue if we have already loaded this resource
            if(_itemsByResourceName.find(entries[i]) != _itemsByResourceName.end())
                continue;

            GFFData gffData;
            if(!gffData.loadFromFile(overridePaths[i]+"/"+entries[i]))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadItemBlueprints() - failed to load " + entries[i] + " from " + overridePaths[i]);
                continue;
            }
            Item* item = new Item();
            if(!item->loadFromGFFData(gffData))
            {
                ErrorUtils::logErrorToConsole("BlueprintManager::loadItemBlueprints() - failed to create Item for " + entries[i]);
                delete item;
                continue;
            }
            if(item->templateResRef()!="" &&
               _itemsByResRef.find(item->templateResRef()) != _itemsByResRef.end())
            {
                // an item with this resref but a different resourcename has already been found!
                // log error and do not add this item.
                ErrorUtils::logErrorToConsole("BlueprintManager::loadItemBlueprints() - The ResRef " + item->templateResRef() + " is already used by another resource!");
                delete item;
                continue;
            }
            _itemsByResourceName[entries[i]]=item;
            _itemsByResRef[item->templateResRef()]=item;
        }
    }

    // Try to load the blueprint from the campaign referenced by the module;
    //_ifoData->capaignid
    //std::string campaignPath=getNWN2Datapath()+"hak/"+hakName;

    // Try to load the blueprint from the module

    if(ModuleManager::instance().modFile() == "")
    {
        // module dir
    }
    else
    {
        // module file
        std::string resourceList;
        //if(!IEncapsulatedResourceFile::getFileListForResourceTypeFromERF())
        //    bool crap = true;
    }

    // Try to load the blueprint from the templates


    return true;
}

bool  BlueprintManager::loadPlaceableBlueprints()
{
    return true;
}

bool  BlueprintManager::loadSoundBlueprints()
{
    return true;
}
bool  BlueprintManager::loadStoreBlueprints()
{
    return true;
}

bool  BlueprintManager::loadTriggerBlueprints()
{
    return true;
}

bool  BlueprintManager::loadWaypointBlueprints()
{
    return true;
}
