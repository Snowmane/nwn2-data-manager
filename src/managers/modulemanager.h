#ifndef MODULEMANAGER_H
#define MODULEMANAGER_H

#include <string>
#include <map>
#include <set>

#include "../data/games-objects/item.h"

class IFOData;

class ModuleManager
{
private:
    ModuleManager();
    ~ModuleManager(){}

public:
    static ModuleManager& instance();

    bool setModPath(std::string path);
    std::string modPath() const {return _modPath;}
    std::string modFile() const {return _modFile;}
    std::string getNWN2Datapath() const;
    const IFOData* ifoData() const {return _ifoData;}
    bool isLoaded() const {return _ifoData!=NULL;}

    //getModuleIFO
protected:
    std::string _modPath;
    std::string _modFile;
    IFOData* _ifoData;

    void resetMembers();
};

#endif // MODULEMANAGER_H
