#include "tdamanager.h"

#include <stdlib.h>
#include "../data/file-formats/gffdata.h"
#include "../data/file-formats/ifodata.h"
#include "../utils/errorutils.h"
#include "../utils/fileutils.h"
#include "../utils/stringutils.h"
#include "modulemanager.h"
#include "resourcemanager.h"

TDAManager::TDAManager()
{
}

TDAManager& TDAManager::instance()
{
    static TDAManager inst;
    return inst;
}

bool TDAManager::get2DARowCount(std::string fileName,
                                DWORD& countOut)
{
    if(_2DAData.find(fileName) == _2DAData.end())
    {
        if(!load2DAFile(fileName))
        {
            countOut=0;
            return false;
        }
    }
    countOut = _2DAData[fileName].numRows();
    return true;
}

void TDAManager::setNWN2Path(std::string path)
{
    char lastChar = StringUtils::lastChar(path);
    if( lastChar != '/' && lastChar != '\\')
            path += "/";

    _NWN2Path = path;
    _2DAData.clear();
}

bool TDAManager::get2DAValue(std::string fileName,
                             std::string rowIndex,
                             std::string colName,
                             std::string& valueOut)
{
    return get2DAValue(fileName, atol(rowIndex.c_str()),colName,valueOut);
}

bool TDAManager::get2DAValue(std::string fileName,
                             DWORD rowIndex,
                             std::string colName,
                             std::string& valueOut)
{
    if(_2DAData.find(fileName) == _2DAData.end())
    {
        if(!load2DAFile(fileName))
        {
            valueOut="";
            return false;
        }
    }
    TDAData tdaData = _2DAData[fileName];
    if(!tdaData.getData(rowIndex, colName,valueOut))
        return false;

    return true;
}

bool TDAManager::get2DARow(std::string fileName,
                           DWORD rowIndex,
                           std::map<std::string,std::string>& row)
{
    if(_2DAData.find(fileName) == _2DAData.end())
    {
        if(!load2DAFile(fileName))
        {
            return false;
        }
    }
    row.clear();
    DWORD rowCount;
    if(!get2DARowCount(fileName,rowCount) || rowIndex >= rowCount)
        return false;

    TDAData tdaData = _2DAData[fileName];
    row = tdaData.getRow(rowIndex);
    return true;
}

bool TDAManager::get2DAColNames(std::string fileName,
                    std::vector<std::string>& colNames)
{
    if(_2DAData.find(fileName) == _2DAData.end())
    {
        if(!load2DAFile(fileName))
        {
            return false;
        }
    }
    TDAData tdaData = _2DAData[fileName];
    colNames = tdaData.colnames();
    return true;
}

bool TDAManager::has2DAColName(std::string fileName,std::string colName)
{
    std::vector<std::string> colNames;
    if(!get2DAColNames(fileName,colNames))
        return false;

    for(size_t i = 0; i < colNames.size(); i++)
    {
        if(colNames[i]==colName)
            return true;
    }
    return false;
}

bool TDAManager::load2DAFile(std::string fileName)
{
    // If we haven't already, then look in the mod
    // and get the list of haks and the campaign.
    if(ModuleManager::instance().ifoData() == NULL)
    {
        ErrorUtils::logErrorToConsole("TDAManager::load2DAFile : module not loaded");
        return false;
    }

    char* fileData = NULL;
    size_t fileLength = 0;
    if(ResourceManager::instance().loadResourceFile(fileName, fileData, fileLength))
    {
        TDAData tdaData;
        if(!tdaData.loadFromData(fileData, fileLength))
        {
            delete[] fileData;
            ErrorUtils::logErrorToConsole("TDAManager::load2DAFile : failed to load " + fileName + " from the ResourceManager");
            return false;
        }
        _2DAData[fileName]=tdaData;
        delete[] fileData;
        return true;
    }
    return false;
}
