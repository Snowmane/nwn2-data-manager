#ifndef ITEM_H
#define ITEM_H

#include <vector>
#include "itemproperty.h"
#include "../../types/types.h"
#include "../../types/cexolocstring.h"

class Item
{
public:
    Item();
    virtual ~Item();

    bool loadFromGFFData(const GFFData& gffData);
    std::string toString();

    enum ModelType
    {
        MT_SIMPLE = 0,
        MT_LAYERED = 1,
        MT_COMPOSITE = 2,
        MT_ARMOR = 3
    };

    //getters
    DWORD addCost() const {return _addCost;}
    INT baseItem() const {return _baseItem;}
    BYTE charges() const {return _charges;}
    DWORD cost() const {return _cost;}
    BYTE cursed() const {return _cursed;}
    CExoLocString descIdentified() const {return _descIdentified;}
    CExoLocString description() const {return _description;}
    CExoLocString locName() const {return _locName;}
    BYTE plot() const {return _plot;}
    std::vector<ItemProperty> propertiesList() const {return _propertiesList;}
    WORD stackSize() const {return _stackSize;}
    BYTE stolen() const {return _stolen;}
    CExoString tag() const {return _tag;}
    CResRef templateResRef() const {return _templateResRef;}
    BYTE cloth1Color() const {return _cloth1Color;}
    BYTE cloth2Color() const {return _cloth2Color;}
    BYTE leather1Color() const {return _leather1Color;}
    BYTE leather2Color() const {return _leather2Color;}
    BYTE metal1Color() const {return _metal1Color;}
    BYTE metal2Color() const {return _metal2Color;}
    BYTE modelPart1() const {return _modelPart1;}
    BYTE modelPart2() const {return _modelPart2;}
    BYTE modelPart3() const {return _modelPart3;}
    BYTE armorPartBelt() const {return _armorPartBelt;}
    BYTE armorPartLBicep() const {return _armorPartLBicep;}
    BYTE armorPartLFArm() const {return _armorPartLFArm;}
    BYTE armorPartLFoot() const {return _armorPartLFoot;}
    BYTE armorPartLHand() const {return _armorPartLHand;}
    BYTE armorPartLShin() const {return _armorPartLShin;}
    BYTE armorPartLShou() const {return _armorPartLShoul;}
    BYTE armorPartLThigh() const {return _armorPartLThigh;}
    BYTE armorPartNeck() const {return _armorPartNeck;}
    BYTE armorPartPelvis() const {return _armorPartPelvis;}
    BYTE armorPartRBicep() const {return _armorPartRBicep;}
    BYTE armorPartRFArm() const {return _armorPartRFArm;}
    BYTE armorPartRFoot() const {return _armorPartRFoot;}
    BYTE armorPartRobe() const {return _armorPartRobe;}
    BYTE armorPartRShin() const {return _armorPartRShin;}
    BYTE armorPartRShou() const {return _armorPartRShoul;}
    BYTE armorPartRThigh() const {return _armorPartRThigh;}
    BYTE armorPartTorso() const {return _armorPartTorso;}
    CExoString comment() const {return _comment;}
    BYTE paletteId() const {return _paletteId;}
    FLOAT xOrientation() const {return _xOrientation;}
    FLOAT yOrientation() const {return _yOrientation;}
    FLOAT xPosition() const {return _xPosition;}
    FLOAT yPosition() const {return _yPosition;}
    FLOAT zPosition() const {return _zPosition;}
    DWORD objectId() const {return _objectId;}
    //List varTable() const {return _varTable;}


protected:
    DWORD _addCost;
    INT _baseItem;
    BYTE _charges;
    DWORD _cost;
    BYTE _cursed;
    CExoLocString _descIdentified;
    CExoLocString _description;
    CExoLocString _locName;
    BYTE _plot;
    std::vector<ItemProperty> _propertiesList;
    WORD _stackSize;
    BYTE _stolen;
    CExoString _tag;
    CResRef _templateResRef;

    // These properties are only valid for MT_LAYERED and MT_ARMOR
    BYTE _cloth1Color;
    BYTE _cloth2Color;
    BYTE _leather1Color;
    BYTE _leather2Color;
    BYTE _metal1Color;
    BYTE _metal2Color;

    // These properties are only valid for MT_SIMPLE, MT_COMPOSITE and MT_LAYERED
    BYTE _modelPart1;

    // These properties are only valid for MT_COMPOSITE
    BYTE _modelPart2;
    BYTE _modelPart3;

    // These properties are only valid for MT_ARMOR
    BYTE _armorPartBelt;
    BYTE _armorPartLBicep;
    BYTE _armorPartLFArm;
    BYTE _armorPartLFoot;
    BYTE _armorPartLHand;
    BYTE _armorPartLShin;
    BYTE _armorPartLShoul;
    BYTE _armorPartLThigh;
    BYTE _armorPartNeck;
    BYTE _armorPartPelvis;
    BYTE _armorPartRBicep;
    BYTE _armorPartRFArm;
    BYTE _armorPartRFoot;
    BYTE _armorPartRHand;
    BYTE _armorPartRobe;
    BYTE _armorPartRShin;
    BYTE _armorPartRShoul;
    BYTE _armorPartRThigh;
    BYTE _armorPartTorso;

    // These properties only exist for blue prints
    CExoString _comment;
    BYTE _paletteId;

    // These properties on exist for item instances
    FLOAT _xOrientation;
    FLOAT _yOrientation;
    FLOAT _xPosition;
    FLOAT _yPosition;
    FLOAT _zPosition;

    // These properties only exist for item game instances
    DWORD _objectId;
    //List _varTable

    void initMembers();
};

#endif // ITEM_H
