#include "item.h"

#include "../../utils/errorutils.h"
#include "../../managers/tdamanager.h"

Item::Item()
{
}

Item::~Item()
{
}

bool Item::loadFromGFFData(const GFFData& gffData)
{
    GFFStruct gffStruct;
    if(!gffData.getGFFStruct(0,gffStruct))
        return false;

    initMembers();

    std::vector<GFFField> gffFields = gffData.getFieldsForStruct(gffStruct);
    for(DWORD gffFieldIndex = 0; gffFieldIndex < gffFields.size(); gffFieldIndex++)
    {
        GFFField gffField = gffFields[gffFieldIndex];
        std::string label = gffData.getLabelForField(gffField);
        FieldValue value = gffData.getValueForField(gffField);
        if(label==""){}
        else if(label=="AddCost"){_addCost=value.dwordVal;}
        else if(label=="BaseItem"){_baseItem=value.intVal;}
        else if(label=="Charges"){_charges=value.byteVal;}
        else if(label=="Cost"){_cost=value.dwordVal;}
        else if(label=="Cursed"){_cursed=value.byteVal;}
        else if(label=="DescIdentified"){_descIdentified=value.cExoLocStringVal;}
        else if(label=="Description"){_description=value.cExoLocStringVal;}
        else if(label=="LocName"){_locName=value.cExoLocStringVal;}
        else if(label=="Plot"){_plot=value.byteVal;}
        else if(label=="PropertiesList")
        {
            for(size_t listValueIndex = 0; listValueIndex < value.listVal.size(); listValueIndex++)
            {
                // iterate over the struct ids for this field
                // and load them as item properties
                GFFStruct propertygffstruct;
                if(!gffData.getGFFStruct(value.listVal[listValueIndex],propertygffstruct))
                {
                    ErrorUtils::logErrorToConsole("Item::loadFromGFFData:PropertiesList - unable to get item property struct");
                    continue;
                }
                ItemProperty itemProperty;
                if(itemProperty.loadFromGFFData(gffData,propertygffstruct))
                    _propertiesList.push_back(itemProperty);
            }
        }
        else if(label=="StackSize"){_stackSize=value.wordVal;}
        else if(label=="Stolen"){_stolen=value.byteVal;}
        else if(label=="Tag"){_tag=value.stringVal;}
        else if(label=="TemplateResRef"){_templateResRef=value.stringVal;}
        else if(label=="Cloth1Color"){_cloth1Color=value.byteVal;}
        else if(label=="Cloth2Color"){_cloth2Color=value.byteVal;}
        else if(label=="Leather1Color"){_leather1Color=value.byteVal;}
        else if(label=="Leather2Color"){_leather2Color=value.byteVal;}
        else if(label=="Metal1color"){_metal1Color=value.byteVal;}
        else if(label=="Metal2Color"){_metal2Color=value.byteVal;}
        else if(label=="ModelPart1"){_modelPart1=value.byteVal;}
        else if(label=="ModelPart2"){_modelPart2=value.byteVal;}
        else if(label=="ModelPart3"){_modelPart3=value.byteVal;}
        else if(label=="ArmorPart_Belt"){_armorPartBelt=value.byteVal;}
        else if(label=="ArmorPart_LBicep"){_armorPartLBicep=value.byteVal;}
        else if(label=="ArmorPart_LFArm"){_armorPartLFArm=value.byteVal;}
        else if(label=="ArmorPart_LFoot"){_armorPartLFoot=value.byteVal;}
        else if(label=="ArmorPart_LHand"){_armorPartLHand=value.byteVal;}
        else if(label=="ArmorPart_LShin"){_armorPartLShin=value.byteVal;}
        else if(label=="ArmorPart_LShoul"){_armorPartLShoul=value.byteVal;}
        else if(label=="ArmorPart_LThigh"){_armorPartLThigh=value.byteVal;}
        else if(label=="ArmorPart_Neck"){_armorPartNeck=value.byteVal;}
        else if(label=="ArmorPart_Pelvis"){_armorPartPelvis=value.byteVal;}
        else if(label=="ArmorPart_RBicep"){_armorPartRBicep=value.byteVal;}
        else if(label=="ArmorPart_RFArm"){_armorPartRFArm=value.byteVal;}
        else if(label=="ArmorPart_RFoot"){_armorPartRFoot=value.byteVal;}
        else if(label=="ArmorPart_RHand"){_armorPartRHand=value.byteVal;}
        else if(label=="ArmorPart_Robe"){_armorPartRobe=value.byteVal;}
        else if(label=="ArmorPart_RShin"){_armorPartRShin=value.byteVal;}
        else if(label=="ArmorPart_RShoul"){_armorPartRShoul=value.byteVal;}
        else if(label=="ArmorPart_RThigh"){_armorPartRThigh=value.byteVal;}
        else if(label=="ArmorPart_Torso"){_armorPartTorso=value.byteVal;}
        else if(label=="Comment"){_comment=value.stringVal;}
        else if(label=="PaletteID"){_paletteId=value.byteVal;}
        else if(label=="XOrientation"){_xOrientation=value.floatVal;}
        else if(label=="YOrientation"){_yOrientation=value.floatVal;}
        else if(label=="XPosition"){_xPosition=value.floatVal;}
        else if(label=="YPosition"){_yPosition=value.floatVal;}
        else if(label=="ZPosition"){_zPosition=value.floatVal;}
        else if(label=="ObjectId"){_objectId=value.dwordVal;}
        else if(label=="VarTable")
        {
            //TODO:
        }
        else
        {
            ErrorUtils::logWarningToConsole("Item::loadFromGFFData : unknown Item Property field found {"+label+"}");
        }
    }
    return true;
}

std::string Item::toString()
{
    std::string desc;
    // get the base2da stuff
    std::string label;
    if(!TDAManager::instance().get2DAValue("baseitems.2da",_baseItem,"label",label))
        return "";

    if(label == "armor")
    {
        //TODO: get the row from armorrulestats2da
        DWORD armorRow = 1;
        //look up armor stats
        //ACBONUS	MAXDEXBONUS	ACCHECK	ARCANEFAILURE%	WEIGHT	COST
        std::string acBonus;
        std::string maxDex;
        std::string checkPenalty;
        std::string spellFailure;
        std::string weight;
        std::string cost;

        if(TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"ACBONUS",acBonus) &&
           TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"MAXDEXBONUS",maxDex) &&
           TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"ACCHECK",checkPenalty) &&
           TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"ARCANEFAILURE%",spellFailure) &&
           TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"WEIGHT",weight) &&
           TDAManager::instance().get2DAValue("armorrulestats.2da",_baseItem,"COST",cost))
        {
            //add armor stats to desc
        }
    }
    else
    {
        //look up weapon stats
        std::string numDice;
        std::string dieToRoll;
        std::string critThreat;
        std::string critHitMult;

        if(TDAManager::instance().get2DAValue("baseitems.2da",_baseItem,"NumDice",numDice) &&
           TDAManager::instance().get2DAValue("baseitems.2da",_baseItem,"DieToRoll",dieToRoll) &&
           TDAManager::instance().get2DAValue("baseitems.2da",_baseItem,"CritThreat",critThreat) &&
           TDAManager::instance().get2DAValue("baseitems.2da",_baseItem,"CritHitMult",critHitMult))
        {
            //add weapon stats to desc
        }
    }


    // add the item properties to desc
    for(size_t index = 0; index < _propertiesList.size(); index++)
    {
        desc += _propertiesList[index].toString()+"\n";
    }
    return desc;
}

void Item::initMembers()
{
    _addCost = 0x00000000;
    _baseItem = 0x00000000;
    _charges = 0x00;
    _cost = 0x00000000;
    _cursed = 0x00;
    _descIdentified = CExoLocString();
    _description = CExoLocString();
    _locName = CExoLocString();
    _plot = 0x00;
    _propertiesList.clear();
    _stackSize = 0x0000;
    _stolen = 0x00;
    _tag.clear();
    _templateResRef.clear();

    // These properties are only valid for MT_LAYERED and MT_ARMOR
    _cloth1Color = 0x00;
    _cloth2Color = 0x00;
    _leather1Color = 0x00;
    _leather2Color = 0x00;
    _metal1Color = 0x00;
    _metal2Color = 0x00;

    // These properties are only valid for MT_SIMPLE, MT_COMPOSITE and MT_LAYERED
    _modelPart1 = 0x00;

    // These properties are only valid for MT_COMPOSITE
    _modelPart2 = 0x00;
    _modelPart3 = 0x00;

    // These properties are only valid for MT_ARMOR
    _armorPartBelt = 0x00;
    _armorPartLBicep = 0x00;
    _armorPartLFArm = 0x00;
    _armorPartLFoot = 0x00;
    _armorPartLHand = 0x00;
    _armorPartLShin = 0x00;
    _armorPartLShoul = 0x00;
    _armorPartLThigh = 0x00;
    _armorPartNeck = 0x00;
    _armorPartPelvis = 0x00;
    _armorPartRBicep = 0x00;
    _armorPartRFArm = 0x00;
    _armorPartRFoot = 0x00;
    _armorPartRHand = 0x00;
    _armorPartRobe = 0x00;
    _armorPartRShin = 0x00;
    _armorPartRShoul = 0x00;
    _armorPartRThigh = 0x00;
    _armorPartTorso = 0x00;

    // These properties only exist for blue prints
    _comment.clear();
    _paletteId = 0x00;

    // These properties on exist for item instances
    _xOrientation = 0.0f;
    _yOrientation = 0.0f;
    _xPosition = 0.0f;
    _yPosition = 0.0f;
    _zPosition = 0.0f;

    // These properties only exist for item game instances
    _objectId = 0x00000000;
    //List VarTable
}
