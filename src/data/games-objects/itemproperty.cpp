#include "itemproperty.h"

#include "../../structs/gfffield.h"
#include  "../../utils/errorutils.h"
#include  "../../utils/stringutils.h"
#include "../../managers/tdamanager.h"
#include "../../managers/tlkmanager.h"

ItemProperty::ItemProperty()
{
    initMembers();
}

ItemProperty::~ItemProperty()
{
}

void ItemProperty::initMembers()
{
    _chanceAppear = 0x00;   // Obsolete. Always 100
    _costTable    = 0x00;   // Index into iprpcosttable.2da
    _costValue    = 0x0000;
    _param1       = 0x00;
    _param1Value  = 0x00;
    _param2       = 0x00;   // Obsolete
    _param2Value  = 0x00;   // Obsolete
    _propertyName = 0x0000;
    _subtype      = 0x0000;
}

bool ItemProperty::loadFromGFFData(const GFFData& gffData, const GFFStruct& gffStruct)
{
    initMembers();

    std::vector<GFFField> gffFields = gffData.getFieldsForStruct(gffStruct);

    for(DWORD i = 0; i < gffFields.size(); i++)
    {
        std::string label = gffData.getLabelForField(gffFields[i]);
        FieldValue value = gffData.getValueForField(gffFields[i]);
        if(label==""){}
        else if(label=="ChanceAppear"){_chanceAppear=value.byteVal;}
        else if(label=="CostTable"){_costTable=value.byteVal;}
        else if(label=="CostValue"){_costValue=value.wordVal;}
        else if(label=="Param1"){_param1=value.byteVal;}
        else if(label=="Param1Value"){_param1Value=value.byteVal;}
        else if(label=="Param2"){_param2=value.byteVal;}
        else if(label=="Param2Value"){_param2Value=value.byteVal;}
        else if(label=="PropertyName"){_propertyName=value.wordVal;}
        else if(label=="Subtype"){_subtype=value.wordVal;}
        else
        {
            ErrorUtils::logWarningToConsole("ItemProperty::loadFromGFFData : unknown Item Property field found {"+label+"}");
        }
    }
    return true;
}

std::string ItemProperty::toString()
{
    // property name
    std::string pnStrRef;
    TLKDataElement pnTde;
    std::string propName;
    if(!TDAManager::instance().get2DAValue("itempropdef.2da", _propertyName,"Name",pnStrRef))
    {
        //TODO error
        return "";
    }
    if(!TLKManager::instance().getTlkEntry(pnStrRef,pnTde))
    {
        //TODO error
        return "";
    }
    propName = pnTde.stringValue;

    // subtype
    std::string stResRef;
    std::string st2daName;
    TLKDataElement stTde;
    std::string subtype;
    if(!TDAManager::instance().get2DAValue("itempropdef.2da", _propertyName,"SubTypeResRef",st2daName))
    {
        //TODO error
        return "";
    }
    if(st2daName!="****")
    {
        st2daName = StringUtils::toLower(st2daName)+".2da";
        if(!TDAManager::instance().get2DAValue(st2daName, _subtype,"Name",stResRef))
        {
            //TODO error
            return "";
        }
        if(!TLKManager::instance().getTlkEntry(stResRef,stTde))
        {
            //TODO error
            return "";
        }
        subtype = stTde.stringValue;
    }

    // costtable value
    std::string ctIndex;
    std::string iprp2daName;
    std::string ctResRef;
    TLKDataElement ctTde;
    std::string ctValue;
    if(!TDAManager::instance().get2DAValue("itempropdef.2da", _propertyName,"CostTableResRef",ctIndex))
    {
        //TODO error
        return "";
    }
    if(!TDAManager::instance().get2DAValue("iprp_costtable.2da", ctIndex,"Name",iprp2daName))
    {
        //TODO error
        return "";
    }
    iprp2daName = StringUtils::toLower(iprp2daName)+".2da";
    // we know this call can fail and it's ok if it.
    // does. We simply have no value for ctValue
    if(TDAManager::instance().get2DAValue(iprp2daName, _costValue,"Name",ctResRef))
    {
        if(!TLKManager::instance().getTlkEntry(ctResRef,ctTde))
        {
            //TODO error
            return "";
        }
        ctValue = ctTde.stringValue;
    }

    // param
    std::string paramTableIndex;
    std::string paramNameStrRef;
    std::string paramTableResRef;
    std::string paramValStrRef;
    TLKDataElement paramNameTde;
    TLKDataElement paramValueTde;
    std::string param;

    bool subtypeTableHasParam1ResRefColumn = false;
    if(st2daName!="****")
    {
        if(TDAManager::instance().has2DAColName(st2daName,"Param1ResRef"))
        {
            subtypeTableHasParam1ResRefColumn = true;
            if(!TDAManager::instance().get2DAValue(st2daName, _subtype,"Param1ResRef",paramTableIndex))
            {
                //TODO error
                return "";
            }
        }
    }
    if(!subtypeTableHasParam1ResRefColumn)
    {
        if(!TDAManager::instance().get2DAValue("itempropdef.2da", _propertyName,"Param1ResRef",paramTableIndex))
        {
            //TODO error
            return "";
        }
    }
    if(paramTableIndex != "****")
    {
        if(!TDAManager::instance().get2DAValue("iprp_paramtable.2da", paramTableIndex,"Name",paramNameStrRef))
        {
            //TODO error
            return "";
        }
        if(!TDAManager::instance().get2DAValue("iprp_paramtable.2da", paramTableIndex,"TableResRef",paramTableResRef))
        {
            //TODO error
            return "";
        }

        paramTableResRef = StringUtils::toLower(paramTableResRef)+".2da";
        if(!TDAManager::instance().get2DAValue(paramTableResRef, _param1Value,"Name",paramValStrRef))
        {
            //TODO error
            return "";
        }

        if(!TLKManager::instance().getTlkEntry(paramNameStrRef,paramNameTde))
        {
            //TODO error
            return "";
        }

        if(!TLKManager::instance().getTlkEntry(paramValStrRef,paramValueTde))
        {
            //TODO error
            return "";
        }

        param = paramNameTde.stringValue + ": " + paramValueTde.stringValue;
    }

    std::string  retVal;
    retVal += propName + " : ";
    if(subtype != "")retVal += " ";
    retVal += subtype;
    if(ctValue != "")retVal += " ";
    retVal += ctValue;
    if(param != "")retVal+= " ";
    retVal += param;

    return retVal;
}
