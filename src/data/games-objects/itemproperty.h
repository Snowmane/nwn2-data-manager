#ifndef ITEMPROPERTY_H
#define ITEMPROPERTY_H

#include <string>
#include "../file-formats/gffdata.h"
#include "../../types/types.h"

struct GFFStruct;

class ItemProperty
{
public:
    ItemProperty();
    virtual ~ItemProperty();

    bool loadFromGFFData(const GFFData& gffData, const GFFStruct& gffStruct);
    std::string toString();

protected:
    void initMembers();

    BYTE _chanceAppear; // Obsolete. Always 100
    BYTE _costTable;    // Index into iprpcosttable.2da
    WORD _costValue;
    BYTE _param1;
    BYTE _param1Value;
    BYTE _param2;       // Obsolete
    BYTE _param2Value;  // Obsolete
    WORD _propertyName;
    WORD _subtype;
};

#endif // ITEMPROPERTY_H
