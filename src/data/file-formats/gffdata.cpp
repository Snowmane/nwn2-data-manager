#include "gffdata.h"

#include "../../utils/fileutils.h"

#include <stdlib.h>
#include <cstring>

namespace
{
    unsigned int LABEL_SIZE = 16;
}

GFFData::GFFData() :
    _fileData(NULL)
{
    resetData();
}

GFFData::~GFFData()
{
    if(_fileData != NULL)
        delete[] _fileData;
}

const char *GFFData::fileData() const
{
    return _fileData;
}

bool GFFData::loadFromFile(std::string filePath)
{
    // Verify the file exists. Set error
    // and return false if not found.
    struct stat fileStats;
    if(!FileUtils::fileExists(filePath, &fileStats))
    {
        //TODO: set error "file not found"
        return false;
    }

    if(fileStats.st_size < sizeof(GFFHeader))
    {
        //TODO: set error "file too small"
        return false;
    }

    char* fileData = FileUtils::getFileData(filePath);

    if(fileData==NULL)
    {
        //TODO error
        return false;
    }

    return loadFromData(fileData,fileStats.st_size);
}

bool GFFData::loadFromData(const char* fileData, size_t length)
{
    // If we currently have data, then reset
    // this instance to clear it
    if(_isInitialized)
        resetData();

    // Now that we have the data, let's init
    // this instance
    _fileData = new char[length];
    std::memcpy(_fileData,fileData,length);
    _fileDataSize = length;
    _isInitialized = true;

    // parse the gff data
    if(!(parseHeader() && parseStructs() &&
         parseFields() && parseLabels()))
    {
        resetData();
        return false;
    }

    if(!sanityCheck())
    {
        resetData();
        return false;
    }

    return true;
}

void GFFData::resetData()
{
    if(_fileData != NULL)
        delete[] _fileData;

    _fileData      = NULL;
    _isInitialized = false;
    _structs.clear();
}

bool GFFData::parseHeader()
{
    if(_fileData == NULL)
        return false;

    //parse the header. set error and return false if it can't be parsed.
    _header.fileType           = std::string(_fileData+0,4);
    _header.gffVersion         = std::string(_fileData+4,4);
    _header.structOffset       = *((unsigned int*)(_fileData+8));
    _header.structCount        = *((unsigned int*)(_fileData+12));
    _header.fieldOffset        = *((unsigned int*)(_fileData+16));
    _header.fieldCount         = *((unsigned int*)(_fileData+20));
    _header.labelOffset        = *((unsigned int*)(_fileData+24));
    _header.labelCount         = *((unsigned int*)(_fileData+28));
    _header.fieldDataOffset    = *((unsigned int*)(_fileData+32));
    _header.fieldDataCount     = *((unsigned int*)(_fileData+36));
    _header.fieldIndicesOffset = *((unsigned int*)(_fileData+40));
    _header.fieldIndicesCount  = *((unsigned int*)(_fileData+44));
    _header.listIndicesOffset  = *((unsigned int*)(_fileData+48));
    _header.listIndicesCount   = *((unsigned int*)(_fileData+52));

    return true;
}


bool GFFData::parseStructs()
{
    unsigned int* currStruct;
    GFFStruct structData;

    for(unsigned int index = 0; index < _header.structCount; index++)
    {
        currStruct = (unsigned int*)(_fileData+_header.structOffset + index*3*sizeof(unsigned int));
        structData.type             = currStruct[0];
        structData.dataOrDataOffset = currStruct[1];
        structData.fieldCount       = currStruct[2];
        _structs.push_back(structData);
    }

    return true;
}

bool GFFData::parseFields()
{
    unsigned int* currField;
    GFFField fieldData;

    for(unsigned int index = 0; index < _header.fieldCount; index++)
    {
        currField = (unsigned int*)(_fileData+_header.fieldOffset + index*3*sizeof(unsigned int));
        fieldData.type             = GFFField::GFFFieldType(currField[0]);
        fieldData.labelIndex       = currField[1];
        fieldData.dataOrDataOffset = currField[2];
        _fields.push_back(fieldData);
    }

    return true;
}

bool GFFData::parseLabels()
{
    for(unsigned int index = 0; index < _header.labelCount; index++)
    {
        char* label = _fileData+_header.labelOffset + index*LABEL_SIZE;
        unsigned int numChars = 0;
        while(*(label+numChars)!='\0' && numChars < LABEL_SIZE)
            numChars++;
        _labels.push_back(std::string(label, numChars));
    }

    return true;
}

bool GFFData::sanityCheck()
{
    for(unsigned int i = 0; i < _fields.size(); i++)
    {
        if(_fields[i].labelIndex >= _labels.size())
            return false;
    }

    return true;
}

GFFHeader GFFData::getGFFHeader() const
{
    return _header;
}

bool GFFData::getGFFStruct(size_t index, GFFStruct& gffStruct) const
{
    if(index < _structs.size())
    {
        gffStruct = _structs[index];
        return true;
    }
    return false;
}

bool GFFData::getGFFField(size_t index, GFFField& gffField) const
{
    if(index < _fields.size())
    {
        gffField = _fields[index];
        return true;
    }
    return false;
}

bool GFFData::getGFFLabel(size_t index, std::string& gffLabel) const
{
    if(index < _labels.size())
    {
        gffLabel = _labels[index];
        return true;
    }
    return false;
}

std::vector<GFFField> GFFData::getFieldsForStruct(const GFFStruct &gffStruct) const
{
    std::vector<GFFField> fields;
    if(gffStruct.fieldCount == 1)
    {
        fields.push_back(_fields[gffStruct.dataOrDataOffset]);
    }
    else
    {
        for(unsigned int i = 0; i < gffStruct.fieldCount; i++)
        {
            int offset = _header.fieldIndicesOffset+sizeof(unsigned int)*i;
            void* test = (void*)(_fileData+offset);
            fields.push_back(_fields[*((unsigned int*)(_fileData+_header.fieldIndicesOffset
                                                       +gffStruct.dataOrDataOffset
                                                       +sizeof(unsigned int)*i))]);
        }
    }
    return fields;
}

std::string GFFData::getLabelForField(const GFFField &gffField) const
{
    std::string label;
    if(!getGFFLabel(gffField.labelIndex, label))
        label="";
    return label;
}

FieldValue GFFData::getValueForField(const GFFField &gffField) const
{
    FieldValue value(gffField.type);

    switch(gffField.type)
    {
    case GFFField::FT_CHAR:{value.charVal = CHAR(gffField.dataOrDataOffset & 0x000000ff);break;}
    case GFFField::FT_WORD:{value.wordVal = WORD(gffField.dataOrDataOffset & 0x0000ffff);break;}
    case GFFField::FT_SHORT:{value.shortVal = SHORT(gffField.dataOrDataOffset & 0x0000ffff);break;}
    case GFFField::FT_BYTE:{value.byteVal = BYTE(gffField.dataOrDataOffset & 0x000000ff);break;}
    case GFFField::FT_DWORD:
    case GFFField::FT_Struct:
    {
        value.dwordVal = gffField.dataOrDataOffset;break;
    }
    case GFFField::FT_FLOAT:{value.floatVal = *((float*)(&(gffField.dataOrDataOffset)));break;}
    case GFFField::FT_INT:{value.intVal = *((int*)(&(gffField.dataOrDataOffset)));break;}
    case GFFField::FT_INT64:
    {
        char* data = _fileData+_header.fieldDataOffset+gffField.dataOrDataOffset;
        value.int64Val = *((INT64*)(data));
        break;
    }
    case GFFField::FT_DOUBLE:
    {
        char* data = _fileData+_header.fieldDataOffset+gffField.dataOrDataOffset;
        value.doubleVal = *((DOUBLE*)(data));
        break;
    }
    case GFFField::FT_CExoString:
    {
        char* data = _fileData+_header.fieldDataOffset+gffField.dataOrDataOffset;
        DWORD len = *((DWORD*)(data));
        value.stringVal = std::string(data+sizeof(len),len);
        break;
    }
    case GFFField::FT_CResRef:
    {
        char* data = _fileData+_header.fieldDataOffset+gffField.dataOrDataOffset;
        DWORD numChars =  *((uint8_t*)(data));
        value.stringVal = std::string(data+sizeof(uint8_t),numChars);
        break;
    }
    case GFFField::FT_CExoLocString:
    {
        CExoLocString str;
        str.loadFromData(_fileData+_header.fieldDataOffset+gffField.dataOrDataOffset);
        value.cExoLocStringVal = str;
        break;
    }
    case GFFField::FT_VOID:
    {
        char* data = _fileData+_header.fieldDataOffset+gffField.dataOrDataOffset;
        DWORD len = *((DWORD*)(data));
        value.binaryVal = new char[len];
        std::memcpy(value.binaryVal, data+sizeof(DWORD),len);
        value.binaryLen = len;
        break;
    }
    case GFFField::FT_List:
    {
        // get the struct ids for this list
        char* data = _fileData+_header.listIndicesOffset+gffField.dataOrDataOffset;
        DWORD numIds = *((DWORD*)(data));
        for(DWORD i = 1; i <= numIds; i++)
        {
            data += sizeof(DWORD);
            value.listVal.push_back(*((DWORD*)(data)));
        }
        break;
    }
    case GFFField::FT_DWORD64:
    {
        //do nothing ???
        break;
    }
    case GFFField::FT_UNKNOWN:
    {
        //do nothing ???
        break;
    }
    };

    return value;
}
