#include "tlkdata.h"

#include "../../utils/fileutils.h"
#include <stdint.h>

namespace
{
    constexpr unsigned int SIZEOF_DATA_ELEMENT = 40;
    constexpr unsigned int TLK_HEADER_SIZE = 20;
}

TLKData::TLKData()
{
}

TLKData::~TLKData()
{
}

bool TLKData::loadFileData(std::string filePath)
{
    // Verify the file exists. Set error
    // and return false if not found.
    struct stat fileStats;
    if(!FileUtils::fileExists(filePath, &fileStats))
    {
        //TODO: set error "file not found"
        return false;
    }

    if(fileStats.st_size < sizeof(TLKHeader))
    {
        //TODO: set error "file too small"
        return false;
    }

    char* fileData = FileUtils::getFileData(filePath);

    if(fileData==NULL)
    {
        //TODO error
        return false;
    }

    // Clear any existing entries
    _tlkEntries.clear();

    // Now that we have the data, let's parse it
    if(!(parseHeader(fileData) &&
         parseTLKData(fileData) ) )
    {
        _tlkEntries.clear();
        return false;
    }

    return true;
}

bool TLKData::parseHeader(char* tlkData)
{
    if(tlkData == NULL)
        return false;

    //parse the header. set error and return false if it can't be parsed.
    char* pos = tlkData;
    _header.fileType            = std::string(pos,4);
    pos += 4;
    _header.tlkVersion          = std::string(pos,4);
    pos += 4;
    _header.languageId          = *((uint32_t *)pos);
    pos += sizeof(uint32_t);
    _header.stringCount         = *((uint32_t *)pos);
    pos += sizeof(uint32_t);
    _header.stringEntriesOffset = *((uint32_t *)pos);

    if(_header.stringCount > 0x00FFFFFF)
        return false;

    return true;
}

bool TLKData::parseTLKData(char* tlkData)
{
    TLKDataElement tde;
    for(unsigned int i = 0; i < _header.stringCount; i++)
    {
        char* pos = tlkData+TLK_HEADER_SIZE+i*SIZEOF_DATA_ELEMENT;
        tde.flags = *((uint32_t*)pos);
        pos += sizeof(uint32_t);
        tde.soundResRef = std::string(pos,16);
        pos += 16;
        tde.volumeVariance = *((uint32_t*)pos);
        pos += sizeof(uint32_t);
        tde.pitchVariance = *((uint32_t*)pos);
        pos += sizeof(uint32_t);
        tde.offsetToString = *((uint32_t*)pos);
        pos += sizeof(uint32_t);
        tde.stringSize = *((uint32_t*)pos);
        pos += sizeof(uint32_t);
        tde.soundLength = *((float*)pos);

        tde.stringValue = std::string(tlkData+_header.stringEntriesOffset+tde.offsetToString, tde.stringSize);

        _tlkEntries[i]=tde;
    }
    return true;
}

bool TLKData::getTLKDataElement(unsigned int strRef, TLKDataElement &tde)
{
    if(_tlkEntries.find(strRef)!=_tlkEntries.end())
    {
        tde = _tlkEntries[strRef];
        return true;
    }

    tde.stringValue="";
    return false;
}

void TLKData::reset()
{
    _header.reset();
    _tlkEntries.clear();
}
