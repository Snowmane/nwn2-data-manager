#ifndef TDADATA_H
#define TDADATA_H

#include "../../types/types.h"
#include <map>
#include <string>
#include <vector>

class TDAData
{
public:
    TDAData();
    virtual ~TDAData();

    bool loadFromFile(std::string filePath);
    bool loadFromData(const char* fileData, size_t length);

    const std::vector<std::string>& colnames() const {return _colNames;}
    const std::map<DWORD, std::vector<std::string>>& rowData() const {return _rowData;}
    bool getData(DWORD rowIndex, std::string colName, std::string& data) const;
    std::map<std::string,std::string> getRow(DWORD rowIndex) const;

    unsigned int numRows() const {return _rowData.size();}
    bool loaded() const {return _loaded;}

protected:
    std::vector<std::string> _colNames;
    std::map<DWORD, std::vector<std::string> > _rowData;
    bool _loaded;

    bool parse2DA(const char* start, unsigned int length);
    void clear();
};

#endif // TDAData_H
