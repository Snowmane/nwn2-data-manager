#ifndef IFODATA_H
#define IFODATA_H

#include "../../types/types.h"
#include "../../types/cexolocstring.h"
#include <vector>

class GFFData;

class IFOData
{
public:
    IFOData();
    virtual ~IFOData();

    bool loadFromGFF(const GFFData& gffData);

    // getters
    const WORD& expansionPack() const {return _expansionPack;}
    //const std::set<???>& modAreaList() const {return _modAreaList;}
    //const std::set<???>& modCacheNssList() const {return _modCacheNssList;}
    const INT& modCreatorId() const {return _modCreatorId;}
    const CExoString& modCustomTlk() const {return _modCustomTlk;}
    //const std::set<???>& modCutSceneList() const {return _modCutSceneList;}
    const BYTE& modDawnHour() const {return _modDawnHour;}
    const BYTE& modDuskHour() const {return _modDuskHour;}
    const CExoLocString& modDescription() const {return _modDescription;}
    const CResRef& modEntryArea() const {return _modEntryArea;}
    const FLOAT& modEntryDirX() const {return _modEntryDirX;}
    const FLOAT& modEntryDirY() const {return _modEntryDirY;}
    const FLOAT& modEntryX() const {return _modEntryX;}
    const FLOAT& modEntryY() const {return _modEntryY;}
    const FLOAT& modEntryZ() const {return _modEntryZ;}
    //const std::set<???>& modExapnList() const {return _modExapnList;}
    //const std::set<???>& modGVarList() const {return _modGVarList;}
    const CExoString& modHak() const {return _modHak;}
    const std::vector<CExoString>& modHakList() const {return _modHakList;}
    //BINARY modId() const {return _modId;}
    BYTE modIsSaveGame() const {return _modIsSaveGame;}
    CExoString modMinGameVer() const {return _modMinGameVer;}
    BYTE modMinPerHour() const {return _modMinPerHour;}
    CExoLocString modName() const {return _modName;}
    CResRef modOnAcquirItem() const {return _modOnAcquirItem;}
    CResRef modOnActvtItem() const {return _modOnActvtItem;}
    CResRef modOnClientEntr() const {return _modOnClientEntr;}
    CResRef modOnClientLeav()  const { return _modOnClientLeav;}
    CResRef modOnCutsnAbort() const {return _modOnCutsnAbort;}
    CResRef modOnHeartbeat() const {return _modOnHeartbeat;}
    CResRef modOnModLoad() const {return _modOnModLoad;}
    CResRef modOnModStart() const {return _modOnModStart;}
    CResRef modOnPlrDeath() const {return _modOnPlrDeath;}
    CResRef modOnPlrDying() const {return _modOnPlrDying;}
    CResRef modOnPlrEqItm() const {return _modOnPlrEqItm;}
    CResRef modOnPlrLvlUp() const {return _modOnPlrLvlUp;}
    CResRef modOnPlrRest() const {return _modOnPlrRest;}
    CResRef modOnPlrUnEqItem() const {return _modOnPlrUnEqItem;}
    CResRef modOnSpawnBtnDn() const {return _modOnSpawnBtnDn;}
    CResRef modOnUnAqreItem() const {return _modOnUnAqreItem;}
    CResRef modOnUsrDefined() const {return _modOnUsrDefined;}
    BYTE modStartDay() const {return _modStartDay;}
    BYTE modStartHour() const {return _modStartHour;}
    BYTE modStartMonth() const {return _modStartMonth;}
    CResRef modStartMovie() const {return _modStartMovie;}
    DWORD modStartYear() const {return _modStartYear;}
    CExoString modTag() const {return _modTag;}
    DWORD modVersion() const {return _modVersion;}
    BYTE modXPScale() const {return _modXPScale;}
    CResRef modOnPCLoaded() const {return _modOnPCLoaded;}
    CResRef modOnChat() const {return _modOnChat;}
    //VOID campaignId() const {return _campaignId;}
    //LIST varTable() const {return _varTable;}
    WORD expansionPack2() const {return _expansionPack2;}

protected:
    WORD _expansionPack;
    //std::set<???> _modAreaList;
    //std::set<???> _modCacheNssList;
    INT _modCreatorId;
    CExoString _modCustomTlk;
    //std::set<???> _modCutSceneList;
    BYTE _modDawnHour;
    BYTE _modDuskHour;
    CExoLocString _modDescription;
    CResRef _modEntryArea;
    FLOAT _modEntryDirX;
    FLOAT _modEntryDirY;
    FLOAT _modEntryX;
    FLOAT _modEntryY;
    FLOAT _modEntryZ;
    //std::set<???> _modExapnList;
    //std::set<???> _modGVarList;
    CExoString _modHak;
    std::vector<CExoString> _modHakList;
    //BINARY _modId;
    BYTE _modIsSaveGame;
    CExoString _modMinGameVer;
    BYTE _modMinPerHour;
    CExoLocString _modName;
    CResRef _modOnAcquirItem;
    CResRef _modOnActvtItem;
    CResRef _modOnClientEntr;
    CResRef _modOnClientLeav;
    CResRef _modOnCutsnAbort;
    CResRef _modOnHeartbeat;
    CResRef _modOnModLoad;
    CResRef _modOnModStart;
    CResRef _modOnPlrDeath;
    CResRef _modOnPlrDying;
    CResRef _modOnPlrEqItm;
    CResRef _modOnPlrLvlUp;
    CResRef _modOnPlrRest;
    CResRef _modOnPlrUnEqItem;
    CResRef _modOnSpawnBtnDn;
    CResRef _modOnUnAqreItem;
    CResRef _modOnUsrDefined;
    BYTE _modStartDay;
    BYTE _modStartHour;
    BYTE _modStartMonth;
    CResRef _modStartMovie;
    DWORD _modStartYear;
    CExoString _modTag;
    DWORD _modVersion;
    BYTE _modXPScale;
    CResRef _modOnPCLoaded;
    CResRef _modOnChat;
    //VOID _campaignId
    //LIST _varTable
    WORD _expansionPack2;
};

#endif // IFODATA_H
