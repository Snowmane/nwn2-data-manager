#ifndef TLKDATA_H
#define TLKDATA_H

#include <map>
#include <string>

#include "../../structs/tlkheader.h"
#include "../../structs/tlkdataelement.h"

class TLKData
{
public:
    TLKData();
    virtual ~TLKData();

    bool loadFileData(std::string filePath);
    bool getTLKDataElement(unsigned int strRef, TLKDataElement& tde);

    void reset();

protected:
    std::map<unsigned int,TLKDataElement> _tlkEntries;
    TLKHeader    _header;

    bool parseHeader(char* tlkData);
    bool parseTLKData(char* tlkData);
};

#endif // TLKDATA_H
