#ifndef GFFDATA_H
#define GFFDATA_H

#include "../../structs/gffheader.h"
#include "../../structs/gffstruct.h"
#include "../../structs/gfffield.h"
#include "../../structs/fieldvalue.h"

#include <string>
#include <vector>

/*********************************/
/* Assumption: little endian CPU */
/*********************************/


class GFFData
{
public:
    GFFData();
    virtual ~GFFData();

    bool loadFromFile(std::string filePath);
    bool loadFromData(const char* fileData, size_t length);

    const char* fileData() const;
    const GFFHeader& header() const{return _header;}

    bool isInitialized(){return _isInitialized;}

    GFFHeader getGFFHeader() const;
    bool getGFFStruct(size_t index, GFFStruct& gffStruct) const;
    bool getGFFField(size_t index, GFFField& gffField) const;
    bool getGFFLabel(size_t index, std::string& gffLabel) const;

    std::vector<GFFField> getFieldsForStruct(const GFFStruct& gffStruct) const;
    std::string getLabelForField(const GFFField& gffField) const;

    FieldValue getValueForField(const GFFField& gffField) const;

protected:
    char*        _fileData;
    unsigned int _fileDataSize;
    bool         _isInitialized;
    GFFHeader    _header;

    std::vector<GFFStruct>   _structs;
    std::vector<GFFField>    _fields;
    std::vector<std::string> _labels;


    void resetData();
    bool parseHeader();
    bool parseStructs();
    bool parseFields();
    bool parseLabels();

    bool sanityCheck();
};

#endif // GFFDATA_H
