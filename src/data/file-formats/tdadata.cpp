#include "tdadata.h"

#include "../../utils/fileutils.h"

TDAData::TDAData():_loaded(false)
{
}

TDAData::~TDAData()
{
}

bool TDAData::loadFromFile(std::string filePath)
{
    // Verify the file exists. Set error
    // and return false if not found.
    struct stat fileStats;
    if(!FileUtils::fileExists(filePath, &fileStats))
    {
        //TODO: set error "file not found"
        return false;
    }

    size_t length;
    char* fileData = FileUtils::getFileData(filePath, &length);

    return loadFromData((const char*)fileData, length);
}

bool TDAData::loadFromData(const char* fileData, size_t length)
{
    if(fileData==NULL)
    {
        //TODO error
        return false;
    }

    // If we currently have data, then reset
    // this instance to clear it
    clear();

    // Now that we have the data, let's start parsing

    // header
    if(!parse2DA(fileData, length))
    {
        //TODO error
        return false;
    }

    _loaded = true;
    return true;
}

bool TDAData::parse2DA(const char *stream, unsigned int length)
{
    unsigned int streamIndex = 0;
    bool searching = true; // either we are searching for the start of a value (true) or we are parsing in the value (false)
    bool openQuote = false;
    std::vector<std::string> currRowData;
    unsigned int charCount = 0;
    unsigned int lineIndex = 0;

    while(streamIndex < length)
    {
        const char* currChar = stream+streamIndex;

        if( (*currChar == '\n') || (*currChar == '\r'))
        {
            // If we are on Mac, '\r' is the line end
            // If we are on Linux, '\n' is the line end
            // If we are on windows, the line end is CRLF '\r\n'
            if( (*currChar == '\r') && (*(currChar+1) == '\n') )
                streamIndex++;

            // We should never find a new line while parsing a quote escaped value
            if(openQuote)
            {
                clear();
                return false;
            }

            // If we are parsing a value, add it to the row data
            if(!searching)
                currRowData.push_back(std::string(currChar - charCount, charCount));

            // parse the lines - header, default(if present), and row data
            if(lineIndex == 0)
            {
                // header
                if(currRowData.size() != 2)
                {
                    clear();
                    return false;
                }
                //TO DO - store version?
            }
            else if(lineIndex == 1)
            {
                if(currRowData.size() == 0){/*no default*/}
                else if(currRowData.size() == 1){/*has default*/}
                else
                {
                    // this file is malformed and the colNames have been found instead of a default
                    _colNames = currRowData;
                }
            }
            else if(lineIndex == 2)
            {
                if(_colNames.size()==0)
                {
                    // this line is the column names
                    _colNames = currRowData;
                }
                else
                {
                    // this line is the first row data line because the 2da did not
                    // contain a line for the default data. sanity check the data
                    if(currRowData.size() != _colNames.size()+1)
                    {
                        // our column names and data don't match - fail
                        clear();
                        return false;
                    }
                    else
                    {
                        // add the data
                        currRowData.erase(currRowData.begin());
                        _rowData[_rowData.size()] = currRowData;
                    }
                }
            }
            else
            {
                // some files will have a newline at the end with no data.
                // do nothing and continue if that's the case
                if(currRowData.size() > 0)
                {
                    // this should be a data row.
                    // sanity check the data
                    if(currRowData.size() != _colNames.size()+1)
                    {
                        // our column names and data don't match - fail
                        clear();
                        return false;
                    }
                    else
                    {
                        // add the data
                        currRowData.erase(currRowData.begin());
                        _rowData[_rowData.size()] = currRowData;
                    }
                }
            }
            // reset search values for next line
            currRowData.clear();
            searching = true;
            charCount = 0;
            lineIndex++;
        }
        else if(searching)
        {
            // we are searching for either a non-whitespace character
            // with a quotation mark having a special meaning
            if(!(isspace(*currChar)))
            {
                if(*currChar=='"')
                    openQuote = true;
                else
                    charCount++;
                searching = false;
            }
        }
        else
        {
            // we are searching for either a whitespace character
            // or a close quotation mark
            if( (openQuote &&*currChar=='"') ||
                (!openQuote && (isspace(*currChar))) )
            {
                currRowData.push_back(std::string(currChar - charCount, charCount));
                openQuote = false;
                searching = true;
                charCount=0;
            }
            else
            {
                charCount++;
            }
        }

        // advance the streamIndex
        streamIndex++;
    }
    // some files will not have a newline at the end with no data,
    // so we must validate and add any data still queued

    // add outstanding field to row
    if(charCount != 0)
    {
        currRowData.push_back(std::string(stream + streamIndex - charCount, charCount));
    }
    // add outstanding row to colNames ifthat has not been set
    if(_colNames.size()==0)
    {
        _colNames=currRowData;
    }
    // otherwise add outstanding row to rowData (if it's well-formed)
    else if(currRowData.size()-1 == _colNames.size())
    {
        currRowData.erase(currRowData.begin());
        _rowData[_rowData.size()] = currRowData;
    }
    else if(currRowData.size() != 0)
    {
        //error
        clear();
        return false;
    }
    return true;
}

void TDAData::clear()
{
    _colNames.clear();
    _rowData.clear();
}

// TODO: This can fail - change to return boolean for succeed/fail
bool TDAData::getData(DWORD rowIndex, std::string colName, std::string& data) const
{
    if(rowIndex < _rowData.size())
    {
        for(int colIndex = 0; colIndex < _colNames.size();colIndex++)
        {
            if(_colNames[colIndex] == colName)
            {
                data = _rowData.at(rowIndex).at(colIndex);
                return true;
            }
        }
    }
    return false;
}

std::map<std::string,std::string> TDAData::getRow(DWORD rowIndex) const
{
    std::map<std::string,std::string> row;
    if(rowIndex < _rowData.size())
    {
        for(size_t i = 0; i < _colNames.size(); i++)
            row[_colNames[i]]=_rowData.at(rowIndex).at(i);
    }
    return row;
}
