#include "ifodata.h"

#include "gffdata.h"
#include "../../utils/errorutils.h"

IFOData::IFOData()
{
}

IFOData::~IFOData()
{
}

bool IFOData::loadFromGFF(const GFFData& gffData)
{
    // get the top level struct

    GFFStruct topLevelStruct;

    if(!gffData.getGFFStruct(0,topLevelStruct))
    {
        ErrorUtils::logErrorToConsole("IFOData::loadFromGFF: Unable to retrieve top level struct for MODULE.IFO");
        return false;
    }

    std::vector<GFFField> topLevelFields = gffData.getFieldsForStruct(topLevelStruct);
    for(size_t i = 0; i < topLevelFields.size(); i++)
    {
        GFFField field = topLevelFields[i];
        std::string label = gffData.getLabelForField(field);
        FieldValue value = gffData.getValueForField(field);
        size_t labelSize=label.size();

        // look for expected values
        if(label == "Expansion_Pack"){_expansionPack=value.wordVal;}
        //else if(label == "Mod_Area_list"){}
        else if(label == "Mod_CacheNSSList"){}
        else if(label == "Mod_Creator_ID"){_modCreatorId=value.intVal;}/*deprecated*/
        else if(label == "Mod_CustomTlk"){_modCustomTlk=value.stringVal;}
        else if(label == "Mod_CutSceneList"){/*deprecated*/}
        else if(label == "Mod_DawnHour"){_modDawnHour=value.byteVal;}
        else if(label == "Mod_DuskHour"){_modDuskHour=value.byteVal;}
        else if(label == "Mod_Description"){_modDescription=value.cExoLocStringVal;}
        else if(label == "Mod_Entry_Area"){_modEntryArea=value.stringVal;}
        else if(label == "Mod_Entry_Dir_X"){_modEntryDirX=value.floatVal;}
        else if(label == "Mod_Entry_Dir_Y"){_modEntryDirY=value.floatVal;}
        else if(label == "Mod_Entry_X"){_modEntryX=value.floatVal;}
        else if(label == "Mod_Entry_Y"){_modEntryY=value.floatVal;}
        else if(label == "Mod_Entry_Z"){_modEntryZ=value.floatVal;}
        else if(label == "Mod_Expan_List"){/*deprecated*/}
        else if(label == "Mod_GVar_List"){/*deprecated*/}
        else if(label == "Mod_Hak"){_modHak=value.stringVal;}/*obsolete*/
        else if(label == "Mod_HakList")
        {
            for(DWORD i = 0; i < value.listVal.size(); i++)
            {
                GFFStruct hakListStruct;
                GFFField hakListField;

                if(!gffData.getGFFStruct(value.listVal[i],hakListStruct))
                {
                    ErrorUtils::logErrorToConsole("IFOData::loadFromGFF: Unable to retrieve HAK list struct entry");
                    return false;
                }
                if(hakListStruct.fieldCount != 1)
                {
                    ErrorUtils::logErrorToConsole("IFOData::loadFromGFF: HAK list struct has more than one field");
                    return false;
                }
                hakListField = gffData.getFieldsForStruct(hakListStruct)[0];
                FieldValue hakValue = gffData.getValueForField(hakListField);
                if(hakValue.type != GFFField::FT_CExoString)
                {
                    ErrorUtils::logErrorToConsole("IFOData::loadFromGFF: HAK entry is wrong type");
                    return false;
                }
                _modHakList.push_back(hakValue.stringVal);
            }
        }
        else if(label == "Mod_ID"){}/*binary*/
        else if(label == "Mod_IsSaveGame"){_modIsSaveGame=value.byteVal;}
        else if(label == "Mod_MinGameVer"){_modMinGameVer=value.stringVal;}
        else if(label == "Mod_MinPerHour"){_modMinPerHour=value.byteVal;}
        else if(label == "Mod_Name"){_modName=value.cExoLocStringVal;}
        else if(label == "Mod_OnAcquirItem"){_modOnAcquirItem=value.stringVal;}
        else if(label == "Mod_OnActvtItem"){_modOnActvtItem=value.stringVal;}
        else if(label == "Mod_OnClientEntr"){_modOnClientEntr=value.stringVal;}
        else if(label == "Mod_OnClientLeav"){_modOnClientLeav=value.stringVal;}
        else if(label == "Mod_OnCutsnAbort"){_modOnCutsnAbort=value.stringVal;}
        else if(label == "Mod_OnHeartbeat"){_modOnHeartbeat=value.stringVal;}
        else if(label == "Mod_OnModLoad"){_modOnModLoad=value.stringVal;}
        else if(label == "Mod_OnModStart"){_modOnModStart=value.stringVal;}/*deprecated*/
        else if(label == "Mod_OnPlrDeath"){_modOnPlrDeath=value.stringVal;}
        else if(label == "Mod_OnPlrDying"){_modOnPlrDying=value.stringVal;}
        else if(label == "Mod_OnPlrEqItm"){_modOnPlrEqItm=value.stringVal;}
        else if(label == "Mod_OnPlrLvlUp"){_modOnPlrLvlUp=value.stringVal;}
        else if(label == "Mod_OnPlrRest"){_modOnPlrRest=value.stringVal;}
        else if(label == "Mod_OnPlrUnEqItm"){_modOnPlrUnEqItem=value.stringVal;}
        else if(label == "Mod_OnSpawnBtnDn"){_modOnSpawnBtnDn=value.stringVal;}
        else if(label == "Mod_OnUnAqreItem"){_modOnUnAqreItem=value.stringVal;}
        else if(label == "Mod_OnUsrDefined"){_modOnUsrDefined=value.stringVal;}
        else if(label == "Mod_StartDay"){_modStartDay=value.byteVal;}
        else if(label == "Mod_StartHour"){_modStartHour=value.byteVal;}
        else if(label == "Mod_StartMonth"){_modStartMonth=value.byteVal;}
        else if(label == "Mod_StartMovie"){_modStartMovie=value.stringVal;}
        else if(label == "Mod_StartYear"){_modStartYear=value.dwordVal;}
        else if(label == "Mod_Tag"){_modTag=value.stringVal;}
        else if(label == "Mod_Version"){_modVersion=value.dwordVal;}
        else if(label == "Mod_XPScale"){_modXPScale=value.byteVal;}
        else if(label == "Mod_OnPCLoaded"){_modOnPCLoaded=value.stringVal;}
        //else if(label == "Campaign_ID"){}
        //else if(label == "VarTable"){}
        else if(label == "Expansion_Pack2"){_expansionPack2=value.wordVal;}
        else if(label == "Mod_OnChat"){_modOnChat=value.stringVal;}
        else
        {
            ErrorUtils::logWarningToConsole("IFOData::loadFromGFF : unknown IFO field found {"+label+"}");
        }
    }
    return true;
}
