#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <string>
#include <vector>
#include <sys/stat.h>

namespace FileUtils
{
    enum EntryType{ET_UNKNOWN, ET_OTHER, ET_FILE, ET_DIR};
    bool fileExists(std::string filePath, struct stat* iFileStats = NULL);
    EntryType entryType(std::string itemPath);
    EntryType entryType(struct stat iItemStats);
    char* getFileData(std::string filePath, size_t *length = NULL);
    std::string getFileExtension(std::string filePath);
    std::string getPathToFile(std::string filePath);
    std::vector<std::string> getDirEntries(std::string dirPath, std::string extension);
    std::vector<std::string> getDirEntries(std::string dirPath,
                                           std::vector<std::string> extensions = std::vector<std::string>());
}

#endif // FILEUTILS_H
