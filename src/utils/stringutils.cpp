#include "stringutils.h"

#include <algorithm>

char StringUtils::lastChar(const std::string& str)
{
    return str[str.size()-1];
}

std::string StringUtils::toLower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}
