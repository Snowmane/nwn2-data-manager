#ifndef ERFUTILS_H
#define ERFUTILS_H

#include <string>
#include <set>
#include "../managers/resourcemanager.h"

namespace ErfUtils
{

bool getFileListForResourceTypeFromERF(std::string pathToErfFile,
                                       ResourceManager::ResourceType resourceType,
                                       std::set<std::string>& fileList);
bool getFileListFromERF(std::string pathToErfFile,
                        std::set<std::string>& fileList);

bool getResourceFileFromERF(std::string pathToErfFile,
                    std::string fileName,
                    char*& fileData,
                    size_t& fileLength);

bool getResourceFileFromERF(const char* erfFileData,
                    const std::string& resourceName,
                    const ResourceManager::ResourceType& resourceType,
                    char*& o_resourceFileData,
                    size_t& o_resourceFileLength);
}

#endif // ERFUTILS_H
