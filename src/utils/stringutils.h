#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <string>

namespace StringUtils
{
    char lastChar(const std::string& str);
    std::string toLower(std::string str);
}

#endif // STRINGUTILS_H
