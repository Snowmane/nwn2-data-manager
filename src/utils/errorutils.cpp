#include "errorutils.h"

#define ERROR_NOTIF std::string("ERROR: ")
#define WARN_NOTIF  std::string("WARNING: ")

#include <iostream>

void ErrorUtils::logWarningToConsole(std::string warning)
{
    std::cout << WARN_NOTIF << warning << std::endl;
}

void ErrorUtils::logErrorToConsole(std::string error)
{
    std::cout << ERROR_NOTIF << error << std::endl;
}
