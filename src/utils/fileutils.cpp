#include "fileutils.h"

#include "stringutils.h"
#include "errorutils.h"
#include <dirent.h>
#include <set>

bool FileUtils::fileExists(std::string filePath, struct stat* iFileStats)
{
    bool fileExists;
    if(iFileStats != nullptr)
    {
        fileExists = (0 == stat(filePath.c_str(),iFileStats));
    }
    else
    {
        struct stat fileStats;
        fileExists = (0 == stat(filePath.c_str(),&fileStats));
    }
    return fileExists;
}

FileUtils::EntryType FileUtils::entryType(std::string itemPath)
{
    struct stat itemStats;
    if(!fileExists(itemPath, &itemStats))
        return ET_UNKNOWN;

    return entryType(itemStats);
}

FileUtils::EntryType FileUtils::entryType(struct stat iItemStats)
{
    if( iItemStats.st_mode & S_IFDIR )
        return ET_DIR;
    else if( iItemStats.st_mode & S_IFREG )
        return ET_FILE;

    return ET_OTHER;
}

char* FileUtils::getFileData(std::string filePath, size_t* length)
{
    struct stat fileStats;

    if(!fileExists(filePath,&fileStats))
        return nullptr;

    // Try to open the file for reading. Set error
    // and return nullptr if it can't be opened.
    FILE* pFile = fopen(filePath.c_str(),"rb");
    if(pFile==nullptr)
        return nullptr;
    char* fileData;
    try
    {
        fileData = new char[fileStats.st_size];
    }
    catch(...)
    {
        //TODO handle if me allocation fails
        ErrorUtils::logErrorToConsole("FileUtils::getFileData : unable to allocate memory for "+filePath);
        bool crap =true;
    }
    size_t bytesRead = fread(fileData,1,fileStats.st_size,pFile);
    fclose(pFile);
    if(bytesRead != fileStats.st_size)
    {
        delete[] fileData;
        return nullptr;
    }
    if(length != nullptr)
        *length = bytesRead;

    return fileData;
}

std::string FileUtils::getFileExtension(std::string filePath)
{
    size_t index = filePath.find_last_of(".");
    if(index == std::string::npos)
        return "";
    return filePath.substr(index+1,std::string::npos);
}

std::string FileUtils::getPathToFile(std::string filePath)
{
    size_t fslashi = filePath.find_last_of('/');
    size_t bslashi = filePath.find_last_of('\\');
    size_t slashi = fslashi > bslashi ? fslashi : bslashi;
    std::string path;
    if(slashi > 0)
        path = filePath.substr(0,slashi);

    return path;
}

std::vector<std::string> FileUtils::getDirEntries(std::string dirPath, std::string extension)
{
    std::vector<std::string> extensions;
    extensions.push_back(extension);
    return getDirEntries(dirPath, extensions);
}

std::vector<std::string> FileUtils::getDirEntries(std::string dirPath, std::vector<std::string> extensions)
{
    std::vector<std::string> entries;

    std::set<std::string> lowerCaseExtensions;
    for(size_t i = 0; i < extensions.size(); i++)
        lowerCaseExtensions.insert(StringUtils::toLower(extensions[i]));

    DIR* d;
    struct dirent *dir;
    d = opendir(dirPath.c_str());
    if(d)
    {
      while ((dir = readdir(d)) != nullptr)
      {
          std::string fileName = dir->d_name;
          if(extensions.size()==0)
          {
              entries.push_back(fileName);
          }
          else
          {
              std::string fileExt = StringUtils::toLower(getFileExtension(fileName));
              if(lowerCaseExtensions.find(fileExt) != lowerCaseExtensions.end())
                  entries.push_back(fileName);
          }
      }

      closedir(d);
    }
    return entries;
}
