#ifndef ERRORS_H
#define ERRORS_H

#include <string>

namespace ErrorUtils
{
    void logWarningToConsole(std::string warning);
    void logErrorToConsole(std::string error);
}

#endif // ERRORS_H
