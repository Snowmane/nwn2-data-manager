#include "erfutils.h"

#include "fileutils.h"
#include "stringutils.h"
#include "../structs/erfheader.h"
#include "../structs/erfkey.h"
#include "../structs/erfresource.h"

bool ErfUtils::getFileListForResourceTypeFromERF(std::string pathToErfFile,
                                                 ResourceManager::ResourceType resourceType,
                                                 std::set<std::string>& fileList)
{
    char* erfFileData = FileUtils::getFileData(pathToErfFile);

    if(erfFileData==nullptr)
    {
        //TODO: error
        return false;
    }

    fileList.clear();

    ERFHeader erfHeader(erfFileData);

    // walk the ERFKeys extrating the filenames
    for(DWORD keyIndex = 0; keyIndex < erfHeader.entryCount; keyIndex++)
    {
        if(strncmp((const char*)erfHeader.version, "V1.0",4) == 0)
        {
            ERFKey_1_0 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_0::KEY_SIZE);
            if(key.resourceType == resourceType)
                fileList.insert(key.fileName+"."+ResourceManager::instance().extensionFromResourceType(key.resourceType));
        }
        else if(strncmp((const char*)erfHeader.version, "V1.1",4) == 0)
        {
            ERFKey_1_1 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_1::KEY_SIZE);
            if(key.resourceType == resourceType)
                fileList.insert(key.fileName+"."+ResourceManager::instance().extensionFromResourceType(key.resourceType));
        }
        else
        {
            //TODO: error, unknown type.
        }
    }
    delete[] erfFileData;
    return true;
}

bool ErfUtils::getFileListFromERF(std::string pathToErfFile,
                                  std::set<std::string>& fileList)
{
    char* erfFileData = FileUtils::getFileData(pathToErfFile);

    if(erfFileData==nullptr)
    {
        //TODO: error
        return false;
    }

    fileList.clear();

    ERFHeader erfHeader(erfFileData);

    // walk the ERFKeys extrating the filenames
    for(DWORD keyIndex = 0; keyIndex < erfHeader.entryCount; keyIndex++)
    {
        if(strncmp((const char*)erfHeader.version, "V1.0",4) == 0)
        {
            ERFKey_1_0 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_0::KEY_SIZE);
            fileList.insert(key.fileName+"."+ResourceManager::instance().extensionFromResourceType(key.resourceType));
        }
        else if(strncmp((const char*)erfHeader.version, "V1.1",4) == 0)
        {
            ERFKey_1_1 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_1::KEY_SIZE);
            fileList.insert(key.fileName+"."+ResourceManager::instance().extensionFromResourceType(key.resourceType));
        }
        else
        {
            //TODO: error, unknown type.
        }
    }
    delete[] erfFileData;
    return true;
}

// repeated calls to this method are slow because it loads the entire
// erf file from disk before extracting the resource.
bool ErfUtils::getResourceFileFromERF(std::string pathToErfFile,
                              std::string resourceFileName,
                              char*& o_resourceFileData,
                              size_t& o_resourceFileLength)
{
    o_resourceFileData=nullptr;
    o_resourceFileLength=0;

    resourceFileName = StringUtils::toLower(resourceFileName);
    std::string resourceExt = FileUtils::getFileExtension(resourceFileName);
    ResourceManager::ResourceType resourceType = ResourceManager::instance().resourceTypeFromExtension(resourceExt);
    std::string resourceName = resourceFileName.substr(0,resourceFileName.size()-resourceExt.size()-1);

    char* erfFileData = FileUtils::getFileData(pathToErfFile);

    bool result =
            getResourceFileFromERF(
                erfFileData,
                resourceName, resourceType,
                o_resourceFileData, o_resourceFileLength);

    delete[] erfFileData;
    return result;
}

bool ErfUtils::getResourceFileFromERF(const char* erfFileData,
                              const std::string& resourceName,
                              const ResourceManager::ResourceType& resourceType,
                              char*& o_resourceFileData,
                              size_t& o_resourceFileLength)
{
    if(erfFileData==nullptr)
    {
        //TODO: error
        return false;
    }

    ERFHeader erfHeader(erfFileData);

    // walk the ERFKeys until we find the file
    // we want to until we run out of keys
    for(DWORD keyIndex = 0; keyIndex < erfHeader.entryCount; keyIndex++)
    {
        if(strncmp((const char*)erfHeader.version, "V1.0",4) == 0)
        {
            ERFKey_1_0 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_0::KEY_SIZE);
            if(StringUtils::toLower(key.fileName)==resourceName && key.resourceType == resourceType)
            {
                ERFResource resource = *((ERFResource*)(erfFileData+erfHeader.offsetToResourceList+key.resourceId*sizeof(ERFResource)));
                o_resourceFileData = new char[resource.resourceSize];
                memcpy(o_resourceFileData,erfFileData+resource.offsetToResource,resource.resourceSize);
                o_resourceFileLength = resource.resourceSize;
                return true;
            }
        }
        else if(strncmp((const char*)erfHeader.version, "V1.1",4) == 0)
        {
            ERFKey_1_1 key(erfFileData+erfHeader.offsetToKeyList+keyIndex*ERFKey_1_1::KEY_SIZE);
            if(StringUtils::toLower(key.fileName)==resourceName && key.resourceType == resourceType)
            {
                ERFResource resource = *((ERFResource*)(erfFileData+erfHeader.offsetToResourceList+key.resourceId*sizeof(ERFResource)));
                o_resourceFileData = new char[resource.resourceSize];
                memcpy(o_resourceFileData,erfFileData+resource.offsetToResource,resource.resourceSize);
                o_resourceFileLength = resource.resourceSize;
                return true;
            }
        }
        else
        {
            //TODO: error, unknown type.
        }
    }
    return false;
}

