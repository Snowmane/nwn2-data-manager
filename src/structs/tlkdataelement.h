#ifndef TLKDATAELEMENT_H
#define TLKDATAELEMENT_H

#include <string>

struct TLKDataElement
{
    unsigned int flags;
    std::string soundResRef;
    unsigned int volumeVariance;
    unsigned int pitchVariance;
    unsigned int offsetToString;
    unsigned int stringSize;
    float soundLength;
    std::string stringValue;
};

#endif // TLKDATAELEMENT_H
