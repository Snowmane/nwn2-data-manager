#ifndef GFFFIELD_H
#define GFFFIELD_H

#include "../types/types.h"

struct GFFField
{
    enum GFFFieldType
    {
        FT_UNKNOWN = -1,
        FT_BYTE = 0,
        FT_CHAR = 1,
        FT_WORD = 2,
        FT_SHORT = 3,
        FT_DWORD = 4,
        FT_INT = 5,
        FT_DWORD64 = 6,
        FT_INT64 = 7,
        FT_FLOAT = 8,
        FT_DOUBLE = 9,
        FT_CExoString = 10,
        FT_CResRef = 11,
        FT_CExoLocString = 12,
        FT_VOID = 13,
        FT_Struct = 14,
        FT_List = 15
    };

    GFFFieldType type;
    DWORD labelIndex;
    DWORD dataOrDataOffset;
};
/*
namespace GFFFieldUtil
{
    bool isComplexGffFieldType(GFFFieldType fieldType)
    {
        return (fieldType == DWORD64 ||
                fieldType == INT64 ||
                fieldType == DOUBLE ||
                fieldType == CExoString ||
                fieldType == ResRef ||
                fieldType == CExoLocString ||
                fieldType == VOID ||
                fieldType == Struct ||
                fieldType == List);
    }
}
*/
#endif // GFFFIELD_H
