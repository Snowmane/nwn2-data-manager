#ifndef FIELDVALUE_H
#define FIELDVALUE_H

#include "gfffield.h"
#include "../types/types.h"
#include "../types/cexolocstring.h"
#include <vector>

struct FieldValue
{
    FieldValue(GFFField::GFFFieldType t)
    {
        // init values
        type = t;

        byteVal = 0x00;
        charVal = 0x00;
        wordVal = 0x0000;
        shortVal = 0x0000;
        dwordVal = 0x00000000;
        intVal = 0x00000000;
        dword64Val = 0x0000000000000000;
        int64Val = 0x0000000000000000;
        floatVal = 0x00000000;
        doubleVal = 0x0000000000000000;
        stringVal = "";
        //cExoLocStringVal - object inits itself
        //listVal          - object inits itself
        binaryVal = NULL;
        binaryLen = 0;
    }

    ~FieldValue()
    {

    }

    GFFField::GFFFieldType type;

    BYTE byteVal;
    CHAR charVal;
    WORD wordVal;
    SHORT shortVal;
    DWORD dwordVal;
    INT intVal;
    DWORD64 dword64Val;
    INT64 int64Val;
    FLOAT floatVal;
    DOUBLE doubleVal;
    std::string stringVal;
    CExoLocString cExoLocStringVal;
    std::vector<DWORD> listVal;
    char* binaryVal;
    DWORD binaryLen;
};

#endif // FIELDVALUE_H
