#ifndef GFFSTRUCT_H
#define GFFSTRUCT_H

struct GFFStruct
{
    unsigned int type;
    unsigned int dataOrDataOffset;
    unsigned int fieldCount;
};

#endif // GFFSTRUCT_H
