#ifndef TLKHEADER_H
#define TLKHEADER_H

#include <string>


struct TLKHeader
{
    std::string fileType;
    std::string tlkVersion;
    unsigned int languageId;
    unsigned int stringCount;
    unsigned int stringEntriesOffset;

    TLKHeader()
    {
        reset();
    }

    void reset()
    {
        fileType = std::string();
        tlkVersion = std::string();
        languageId = 0;
        stringCount = 0;
        stringEntriesOffset = 0;
    }
};
#endif // TLKHEADER_H
