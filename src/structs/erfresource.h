#ifndef ERFRESOURCE_H
#define ERFRESOURCE_H

#include "../types/types.h"

struct ERFResource
{
    DWORD offsetToResource;
    DWORD resourceSize;
};

#endif // ERFRESOURCE_H
