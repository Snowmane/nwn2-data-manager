#ifndef ERFKEY_H
#define ERFKEY_H

#include "../types/types.h"
#include <cstring>

struct ERFKey_1_0
{
    static const DWORD KEY_SIZE = 24;

    ERFKey_1_0(const char* data)
    {
        char cFileName[17];
        memcpy(&cFileName, data, 16);
        cFileName[16]='\0';
        fileName=cFileName;
        resourceId   = *((DWORD*)(data+16));
        resourceType = *((WORD*)(data+20));
        unused       = *((WORD*)(data+22));
    }

    std::string fileName;
    DWORD resourceId;
    WORD resourceType;
    WORD unused;

    std::string version;
};

struct ERFKey_1_1
{
    static const DWORD KEY_SIZE = 40;

    ERFKey_1_1(const char* data)
    {
        char cFileName[33];
        memcpy(&cFileName, data, 32);
        cFileName[32]='\0';
        fileName=cFileName;
        resourceId   = *((DWORD*)(data+32));
        resourceType = *((WORD*)(data+36));
        unused       = *((WORD*)(data+38));
    }

    std::string fileName;
    DWORD resourceId;
    WORD resourceType;
    WORD unused;

    std::string version;
};

#endif // ERFKEY_H
