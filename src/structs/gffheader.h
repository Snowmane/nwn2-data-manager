#ifndef GFFHEADER_H
#define GFFHEADER_H

#include <string>

struct GFFHeader
{
    std::string fileType;
    std::string gffVersion;
    unsigned int structOffset;
    unsigned int structCount;
    unsigned int fieldOffset;
    unsigned int fieldCount;
    unsigned int labelOffset;
    unsigned int labelCount;
    unsigned int fieldDataOffset;
    unsigned int fieldDataCount;
    unsigned int fieldIndicesOffset;
    unsigned int fieldIndicesCount;
    unsigned int listIndicesOffset;
    unsigned int listIndicesCount;
};
#endif // GFFHEADER_H
