#ifndef ERFHEADER_H
#define ERFHEADER_H

#include "../types/types.h"
#include <cstring>

struct ERFHeader
{
    ERFHeader(const char* data)
    {
        memcpy(&fileType, data+0, 4);
        memcpy(&version, data+4, 4);
        languageCount           = *((DWORD*)(data+8));
        localizedStringSize     = *((DWORD*)(data+12));
        entryCount              = *((DWORD*)(data+16));
        offsetToLocalizedString = *((DWORD*)(data+20));
        offsetToKeyList         = *((DWORD*)(data+24));
        offsetToResourceList    = *((DWORD*)(data+28));
        memcpy(&buildYear, data+32, 4);
        memcpy(&buildDay, data+36, 4);
        descriptionStrRef       = *((DWORD*)(data+40));
    }

    char fileType[4];
    char version[4];
    DWORD languageCount;
    DWORD localizedStringSize;
    DWORD entryCount;
    DWORD offsetToLocalizedString;
    DWORD offsetToKeyList;
    DWORD offsetToResourceList;
    BYTE buildYear[4];
    BYTE buildDay[4];
    DWORD descriptionStrRef;
};

#endif // ERFHEADER_H
